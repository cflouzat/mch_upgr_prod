# Muon Tracking Chambers Upgrade - scripts

Here are the scripts to control the GRORC/CRU - SOLAR - DUALSAMPA of the MCH detector.
To read these instructions with a command line interface
```
pandoc README.md | lynx -stdin
```
or
```
pandoc -t plain README.md | less
```

# SETUP
### Install the ROC drivers
```
follow O2 instruction 
https://alice-o2.web.cern.ch/node/157
(Install the alisw-flpproto+v20171011-1 version of the software)
```
after this is done configure the bash in the following way
```
export MODULEPATH=/opt/alisw/el7/modulefiles:$MODULEPATH
eval `modulecmd bash load flpproto/v20171011-1`
export PYTHONPATH=$PYTHONPATH:$FLPPROTO_ROOT/lib:/opt/alisw/el7/ReadoutCard/v0.6.0-1/lib
```

# USE OF GIT

### to get a local copy of the git repo
```
git clone git@drf-gitlab.cea.fr:cflouzat/alice-mch.git
```
or
```
git clone https://gitlab.cern.ch/cflouzat/mch_upgr_prod.git
```
if the command failed for a problem of credentials, this probably means the computer is not identified

### generate a private/public key 
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
strike <ENTER> for all questions and copy the content of the ~/.ssh/id_rsa.pub (with no carriage return) into clipboard

go to you gitlab interface (here it is https://drf-gitlab.cea.fr/profile/keys) and add a new key set with the content of your clipboard then you can retry the git clone line...


### update your local copy of the git repo
```
git pull
```
if it complains about your local changes:
1/ you commit them, or merge them with the new set (solve the  conflicts)
2/ or you disccard your local changes and overwrite your local copy with the new version

### remove all local changes

To remove all local changes from your working copy, simply stash them:
```
git stash save --keep-index
```
...  and as you don't need them anymore, you now can drop that stash:
```
git stash drop
```
and then
```
git pull
```

### commit and update

first check the status to avoid forgetting the addition of changed file in your commit
```
git status
```
if files or directories are missing
```
git add myfile.go
```
to commit
```
git commit -m "some intersting comments"
```
to upload to remote git repository (in master branch)
```
git push origin master
```
