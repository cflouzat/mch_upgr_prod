#!/usr/bin/python3
# Tkinter and GPIO together 
# With class structure.

from CColor import *
from CTestSolarI2c import	*
from tkinter import *
#import RPi.GPIO as GPIO
import time

class CQRCode:
	def __init__ (self,strName):
		self.bValid=False
		self.strName=strName
		self.strValue=""

class MyApp(Frame):
	def __init__ (self, parent):
		self._testSolarI2c=CTestSolarI2c(True)

		self.myParent = parent
		self.myframe = Frame(parent)

		self._QRCodeDcdc_1_5V=CQRCode("DCDC 1.5V")
		self._QRCodeDcdc_2_5V=CQRCode("DCDC 2.5V")
		self._QRCodeSolar=CQRCode("SOLAR")


		# Set up the frame geometry padding. 
		self.myframe.pack(padx="2m",pady="1m",ipadx="2m",ipady="1m")

		ch=200
		cw=350
		self.canvas1 = Canvas(self.myframe, height=ch, width=cw)
		dx=cw/2
		dy=0
		self.txtTitle		= self.canvas1.create_text(dx,	dy,width=cw, anchor="n", text="Test cartes SOLAR", font=("Purisa", 12))



		dx=90
		dy=dy+5

		dy=dy+30
		self.txtPCB			= self.canvas1.create_text(dx,			dy,width=140, anchor="ne", text="Numero PCB")
		self.ledPCB			= self.canvas1.create_oval(dx+5,		dy,dx+5+15,dy+15, fill="red")
		self.IdPCB			= self.canvas1.create_text(dx+5+15+5,	dy,width=140, anchor="nw", text="")

		dy=dy+30
		self.txtSOLAR		= self.canvas1.create_text(dx,			dy,width=140, anchor="ne", text=self._QRCodeSolar.strName)
		self.ledSOLAR		= self.canvas1.create_oval(dx+5,		dy,dx+5+15,dy+15, fill="red")
		self.IdSOLAR		= self.canvas1.create_text(dx+5+15+5,	dy,width=140, anchor="nw", text="")

		dy=dy+30
		self.txtDCDC1_5V	= self.canvas1.create_text(dx,			dy,width=140, anchor="ne", text=self._QRCodeDcdc_1_5V.strName)
		self.ledDCDC1_5V	= self.canvas1.create_oval(dx+5,		dy,dx+5+15,dy+15, fill="red")
		self.IdDCDC1_5V		= self.canvas1.create_text(dx+5+15+5,	dy,width=140, anchor="nw", text="")

		dy=dy+30
		self.txtDCDC2_5V	= self.canvas1.create_text(dx,			dy,width=140, anchor="ne", text=self._QRCodeDcdc_2_5V.strName)
		self.ledDCDC2_5V	= self.canvas1.create_oval(dx+5,		dy,dx+5+15,dy+15, fill="red")
		self.IdDCDC2_5V		= self.canvas1.create_text(dx+5+15+5,	dy,width=140, anchor="nw", text="")

		self.canvas1.pack()
		
		self.hdlBtn=Button(parent,text="Lancer le Test Fonctionnel",state=DISABLED,command=self.runTest) # or normal
		self.hdlBtn.pack()

		self.cmdline=Entry(parent)
		self.cmdline.bind("<Return>",self._evalcmd)
		self.cmdline.pack()
		self.cmdline.focus_set()

	def _evalcmd(self,event):
		strings=self.cmdline.get()
		strList=strings.split(',')
		if(len(strList)==3):
			if(strList[0]=='SOLAR'):
				self.canvas1.itemconfig(self.ledSOLAR, fill="green")
				self.canvas1.itemconfig(self.IdSOLAR,text="%s"%strList[1])
			elif(strList[0]=='FEASTMP'):
				if(strList[2]=='1.5'):
					self.canvas1.itemconfig(self.ledDCDC1_5V, fill="green")
					self.canvas1.itemconfig(self.IdDCDC1_5V,text="%s"%strList[1])
				elif(strList[2]=='2.5'):
					self.canvas1.itemconfig(self.ledDCDC2_5V, fill="green")
					self.canvas1.itemconfig(self.IdDCDC2_5V,text="%s"%strList[1])
				else:
					self.canvas1.itemconfig(self.ledDCDC1_5V, fill="red")
					self.canvas1.itemconfig(self.ledDCDC2_5V, fill="red")
			else:
				self.canvas1.itemconfig(self.ledSOLAR, fill="red")
				self.canvas1.itemconfig(self.ledDCDC1_5V, fill="red")
				self.canvas1.itemconfig(self.ledDCDC2_5V, fill="red")
		else:
			try:
				nPcbNumber=int(strings)
				self.canvas1.itemconfig(self.ledPCB, fill="green")
				self.canvas1.itemconfig(self.IdPCB,text="%d"%nPcbNumber)
				self.hdlBtn.config(state=NORMAL)
			except:
				self.canvas1.itemconfig(self.ledPCB, fill="red")
				self.canvas1.itemconfig(self.IdPCB,text="Invalide")
			self.cmdline.delete(0,'end')
	
	def runTest(self):
		if(self._testSolarI2c.bStatus==True):
			self._testSolarI2c.init()
			self._testSolarI2c.start(0)
			strUniqueID=self._testSolarI2c.read()
			print("unique id is %s %sOK%s"%(strUniqueID,CColor.GREEN,CColor.END))
			for i in range(1,9):
				self._testSolarI2c.start(i)
				if(self._testSolarI2c.read()!="ok"):
					print("%sERROR%s: i2c number %d error"%(CColor.RED,CColor.END,i))
				else:
					print("I2c Test port %d %sOK%s"%(i,CColor.GREEN,CColor.END))




# Main loop Tk link and class stuff.
root = Tk()
root.title("LED/SWITCH")
myapp = MyApp(root)
root.mainloop()
