// $Id: datefake.h,v 1.3 2008/01/31 11:40:45 alberto Exp $
//
// DAQ fake include
//
#ifndef _datefake_h_
#define _datefake_h_

#ifndef __CINT__
#ifdef __cplusplus
extern "C" {
#endif
#endif

#ifdef NO_DATE
int monitorSetDataSource(char *input);
int monitorDeclareMp(char *mp);
char *monitorDecodeError( int error ); 
int monitorGetEventDynamic(void **ptr);
int monitorDeclareTable( char** );
#else
  //#include "monitor.h"
#endif

#ifndef __CINT__
#ifdef __cplusplus
}
#endif
#endif

#endif
