// $Id: swap.h,v 1.2 2007/04/26 13:06:37 albertobaldisseri Exp $
//
// MuTrkOnline
// Swaping data
// A: Baldisseri : Feb. 2006
//
//
void SetSwapFlag(int fl);
int SwapNeeded();
int Swap (int* input, int nwords );

