// $Id: datefake.cxx,v 1.3 2008/01/31 11:40:45 alberto Exp $
//
// Simulating the DATE V5 routines
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "eventD7-106.h"
#include "swap.h"

static FILE *fpin;

#ifndef __CINT__
#ifdef __cplusplus
extern "C" {
#endif
#endif
#ifdef NO_DATE
int monitorSetDataSource(char *input)
{
  std::cout << "Opening file : " << input << std::endl;
  fpin = fopen(input,"r"); // Testing evt from pointer
  if (!fpin)
    {
      std::cout << "ERROR : can't open file " << input << std::endl;
      return -1;
    }

  return 0;
}
int monitorDeclareMp(char *mp)
{
  std::cout << "monitorDeclareMp : " << mp << std::endl;
  return 0; 
}

char* monitorDecodeError(int error)
{
//  return '\0';
  return 0;

}

int monitorGetEventDynamic(void **ptr)
{
  // Read the DATE header
  struct eventHeaderStruct dateheader;
  int words = 0;
  int* ptr_int;
  int hsize = sizeof(struct eventHeaderStruct)/4; // header size in 32-bits words
  words += fread(&dateheader,sizeof(int),hsize,fpin);
  if (!words) return -1; // exit (EOF)
  // Swap the header if needed
  if (dateheader.eventMagic == EVENT_MAGIC_NUMBER) 
    SetSwapFlag(0);
  else
    SetSwapFlag(1);
  if (SwapNeeded()) Swap((int*)(&dateheader),hsize);

//   std::cout<<"SwapNeeded "<<SwapNeeded()<<std::endl;
  
  // buffer allocation + header filling
//   int* databuf = new int[dateheader.eventSize/4];
  (*ptr) = malloc(dateheader.eventSize);
  ptr_int = (int*) (*ptr);
  memcpy(ptr_int,&dateheader,hsize*4);
  
  //Read the remain data
  words += fread(ptr_int+hsize,sizeof(int),dateheader.eventSize/4-hsize,fpin);
  if (SwapNeeded()) Swap(ptr_int+hsize,dateheader.eventSize/4-hsize);

  return words;
}

int monitorDeclareTable(char** table)
{
  std::cout << "monitorDeclareTable : " << table << std::endl;
  return 0;
}
#endif

#ifndef __CINT__
#ifdef __cplusplus
}
#endif
#endif
