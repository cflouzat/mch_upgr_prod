#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <iostream>



#ifdef NO_DATE
#include "datefake.h"
#include "eventD7-106.h"
#else
#include <monitor.h>
#endif

#include "decode.h"
#include "swap.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

struct RawEvent
{
  int*                                 fRawEventPtr;            // Raw event pointer
  struct eventHeaderStruct*            fDateHeaderPtr;          // DATE header
  struct equipmentHeaderStruct*        fDateEquipmentHeaderPtr; // DATE equipment header
  int                                  fParityErrors;           // Number of parity errors
  int                                  fGlitchErrors;           // Number of glitch errors
  int*                                 fSampaData;              // Table of pointers to each SOLAR port
  DualSampa ds[40];
  void Init();
  int Fill(int* buf);
  void DumpRawEvent();
  void DumpDateHeader();
  void DumpDateEquipmentHeader();
};


void RawEvent::Init()
{
  for(int i = 0; i < 40; i++) {
    DualSampaInit( &(ds[i]) );
    ds[i].id = i;
  }
}


int RawEvent::Fill(int* buf)
{
  // Set the raw event pointer
  fRawEventPtr = buf;

  // Set the current pointer (to decode the event)
  int* bufpt = fRawEventPtr;
  // Low level print (only the buffer)
  if (gPrintLevel == -1)
  {
    DumpRawEvent();
    return (*buf)/4; // return the size in words
  }

  //    if (0) // BUG TO BE CORRECTED
  //    {
  //        DumpRawSampaEvent();
  //        return (*buf)/4; // return the size in words
  //    }

  // Set DATE header (first one GDC or LDC)
  fDateHeaderPtr = (struct eventHeaderStruct*) bufpt;
  //   if (gPrintLevel) DumpDateHeader();

  // Non physics/calibration events
  if (fDateHeaderPtr->eventType != physicsEvent &&
      fDateHeaderPtr->eventType != calibrationEvent )
  {
    if (fDateHeaderPtr->eventType == endOfRun)
    {
      std::cout << ">>><<< End of Run record found >>><<<" << std::endl;
      DumpDateHeader();
      return -1; // exit

    }
    else if (fDateHeaderPtr->eventType == startOfRun)
    {
      if (gPrintLevel >= 1) {
      std::cout << ">>><<< Start of Run record found >>><<<" << std::endl;
      DumpDateHeader();
      }
      return 0; // skip
    }
    else if (fDateHeaderPtr->eventType == startOfRunFiles)
    {
      //
      // We suppose here that the DATE header is pointing to the LDC (no GDC)
      //
      if (gPrintLevel >= 1) {
      std::cout << ">>><<< Start of Run Files record found >>><<<" << std::endl;
      DumpDateHeader();
      }
      static int counter = 1;
      // Output the SOR files
      FILE *fp;
      char sorfile[256];
      sprintf(sorfile,"sorfile%d-%d",counter,fDateHeaderPtr->eventRunNb);
      fp = fopen(sorfile,"w");
      // Swap (already done but needed again for ASCII) : ASCII is machine independent
      if (SwapNeeded()) Swap(bufpt+fDateHeaderPtr->eventHeadSize/4,
          (fDateHeaderPtr->eventSize-fDateHeaderPtr->eventHeadSize)/4);
      // Look for 0x00 ASCI code and replace it by carriage return
      char* charptr = (char*) (bufpt+fDateHeaderPtr->eventHeadSize/4);
      for (int ichar = 0; ichar < fDateHeaderPtr->eventSize-fDateHeaderPtr->eventHeadSize; ichar++)
        if (((int)charptr[ichar]) == 0) charptr[ichar] = '\n';

      // integer
      fwrite(bufpt+fDateHeaderPtr->eventHeadSize/4,4,
          (fDateHeaderPtr->eventSize-fDateHeaderPtr->eventHeadSize)/4,fp);
      // bytes
      //    fwrite(bufpt+fDateHeaderPtr->eventHeadSize/4,1,
      //     fDateHeaderPtr->eventSize-fDateHeaderPtr->eventHeadSize,fp);
      fclose(fp);
      counter++;

      return 0; // skip
    }
    else
    {
      std::cout << "ERROR : Unknown record type = " << fDateHeaderPtr->eventType << std::endl;
      DumpDateHeader();
      return 0; // skip
    }
  }

  // Physics events
  int eventwords = 0;
  int ldcwords = 0;
  int eventsize = 0;
  // GDC with LDC's following ?
  if (fDateHeaderPtr->eventGdcId != VOID_ID)
  { // Starting by a GDC header -> skip it
    if (gPrintLevel) DumpDateHeader();
    bufpt+= (fDateHeaderPtr->eventHeadSize)/4 ; // DATE header size in 32-bits words;
    // Set the LDC header
  }
  eventsize = (fDateHeaderPtr->eventSize)/4;      // event size in 32-bits words;
  eventwords+=(fDateHeaderPtr->eventHeadSize)/4 ; // DATE header size in 32-bits words; ;

  // LDC loop
  while (eventwords < eventsize)
  {

    fDateHeaderPtr = (struct eventHeaderStruct*) bufpt;   // Set the DATE header to the LDC one
    if (gPrintLevel) DumpDateHeader();
    int ldcsize  = (fDateHeaderPtr->eventSize)/4;      // event size in 32-bits words
    int ldchsize = (fDateHeaderPtr->eventHeadSize)/4 ; // DATE header size in 32-bits words
    // Skip the LDC header (WE SUPPOSE HERE THAT THE LDC IS THE MUTRK ONE)
    bufpt+= ldchsize;
    ldcwords = 0;
    ldcwords+= ldchsize;

    // event words update
    eventwords += ldcsize;

    // Equipment loop
    while (ldcwords < ldcsize)
    {
      // DATE Equipment Header
      fDateEquipmentHeaderPtr = (struct equipmentHeaderStruct*) bufpt;
      if (gPrintLevel >= 2) DumpDateEquipmentHeader();


      // MUON Tracking equipment (20 Equipment : 0xA00  -> 0xA13) ?
      //  if ((fDateEquipmentHeaderPtr->equipmentId < 0xA00 || fDateEquipmentHeaderPtr->equipmentId > 0xA13)
      //      && fDateEquipmentHeaderPtr->equipmentId != 212)
      //    {
      //      // If it not a MUON Tracking Equipment -> skip it
      //      bufpt += fDateEquipmentHeaderPtr->equipmentSize/4;
      //      ldcwords += fDateEquipmentHeaderPtr->equipmentSize/4;
      //      continue;
      //    }

      // Skip Equipment header
      bufpt+= sizeof(struct equipmentHeaderStruct)/4;  // NO SIZE IS INDICATED IN THE HEADER
      ldcwords+= sizeof(struct equipmentHeaderStruct)/4;  // IS THAT CORRECT ?

      // NOT VALID IF WE HAVE MORE THAN ONE EQUIPMENT IN THE LDC
      if (0/*fDateEquipmentHeaderPtr->equipmentSize != fDateHeaderPtr->eventSize - fDateHeaderPtr->eventHeadSize*/)
      {
        std::cout<<"MuTrkRawEvent::Fill : ERROR Equipment size "<<fDateEquipmentHeaderPtr->equipmentSize
            <<" not coherent with the DATE header "<<fDateHeaderPtr->eventSize<<std::endl;
        return 0;
      }

      // Skip the StartOfPacket (SOP): 4 words
      if (gPrintLevel >= 2) printf(" - StartOfPacket (4 words): 0x%X (%d) 0x%X (%d) 0x%X (%d) 0x%X (%d)\n",*bufpt,*bufpt,*(bufpt+1),*(bufpt+1),
          *(bufpt+2),*(bufpt+2),*(bufpt+3),*(bufpt+3));
      if ((*bufpt != 0) || (*(bufpt+1) != 0) || (*(bufpt+2) != 0) || (*(bufpt+3) != 0x1)) // Discard the event if SOP is wrong
      {
        //      printf("ERROR: Bad StartOfPacket: 0x%x 0x%x 0x%x\n",*bufpt,*(bufpt+1),*(bufpt+2));
        if (gPrintLevel >= 1) printf("ERROR: Bad StartOfPacket: 0x%x 0x%x 0x%x 0x%x\n",*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));

        //      printf("Skipping the event!\n");
        //      return 0;

      }
      bufpt+= 4;
      ldcwords+= 4;

      // Skip the first two GBT words
      if (gPrintLevel >= 1) printf("WARNING: Skiping: 0x%x 0x%x 0x%x 0x%x\n",*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));
      bufpt+= 4;
      ldcwords+= 4;
      if (gPrintLevel >= 1) printf("WARNING: Skiping: 0x%x 0x%x 0x%x 0x%x\n",*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));
      bufpt+= 4;
      ldcwords+= 4;
      if (gPrintLevel >= 1) printf("WARNING: First data word: 0x%x 0x%x 0x%x 0x%x\n",*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));

      //
      // MUON Tracking Payload
      //
      //  int payloadWords = fDateEquipmentHeaderPtr->equipmentSize/4-11;
      //  int gbtWords = payloadWords/4;
      int payloadWords = fDateEquipmentHeaderPtr->equipmentSize/4-7-3*4;
      int gbtWords = payloadWords/4;
      if (gPrintLevel >= 2) std::cout<<"   MCH Payload size "<<payloadWords
          <<" 32-bits words "<<gbtWords<<" 80-bits (96) GBT words"<<std::endl;



      //  fSampaHeaderPtr = (struct Sampa::SampaHeaderStruct*) new int[2*gbtWords/32];
      fSampaData = new int[2*gbtWords/32+1];
      //        fSampaData = new int[512];

      //  static unsigned long data64 = 0;
      static unsigned int data10 = 0;
      static int ibit10 = 0;
      //  static int ibit50 = 0;
      //  static int dataSynchronized = 0;
      //  static int headerToRead = 0;
      //  static int dataToRead = 0;
      //  static int clusterSizeToRead = 0;
      //  static int clusterTimeToRead = 0;
      //  static int clusterDataCount = 0;  // current number of data in a cluster
      //  static unsigned long powerMultiplier = 1;


      //  static int firstDataWord = 1;
      //  int port0Data; // 2 bits fo data for Port 0
      int igbt = 0;
      //  int nsyn2Bits = 0;
      //  int nSync = 0; // number of Sync words found
      //  static Sampa::SampaHeaderStruct* sampaHeader = new (Sampa::SampaHeaderStruct);
      //  int nBitsInDateEvent = gbtWords*2; // number of bits from GBT data in the current DATE event
      while (igbt < gbtWords)
      {
        if (gPrintLevel == -3) // ONLY RAW PRINT
        {
          static int counter = 0;

          // Read 10 bits words
          data10 += ((*(bufpt+2))&0x1)<<ibit10;
          ibit10++;
          data10 += (((*(bufpt+2))>>1)&0x1)<<ibit10;
          ibit10++;

          if (ibit10 == 10)
          {
            counter++;
            printf("SAMPA Data word %d: 0x%X (%d)\n",counter,data10,data10);
            ibit10 = 0;
            data10 = 0;
          }
          igbt++;
          bufpt+= 4;
          ldcwords+= 4;
          continue;
        }

        if (gPrintLevel == -2) // ONLY RAW PRINT
        {
          printf("GBT word # %d Hex: %08x %08x %08x %08x\n",igbt+1,*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+1))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+2))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+3))>>i)&0x1);
          printf("\n");
          igbt++;
          bufpt+= 4;
          ldcwords+= 4;
          continue;
        }

        if (gPrintLevel >= 4)
        {
          printf("GBT word # %d Hex: %08x %08x %08x %08x\n",igbt+1,*bufpt,*(bufpt+1),*(bufpt+2),*(bufpt+3));
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+1))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+2))>>i)&0x1);
          printf(" ");
          for (int i = 31; i >= 0; i--) printf("%d",((*(bufpt+3))>>i)&0x1);
          printf("\n");
        }


        if (igbt == gbtWords -1)
        {
          if (gPrintLevel >= 2) printf(" - EndOfPacket (4 words): 0x%X (%d) 0x%X (%d) 0x%X (%d) 0x%X (%d)\n",*bufpt,*bufpt,*(bufpt+1),*(bufpt+1),*(bufpt+2),*(bufpt+2),*(bufpt+3),*(bufpt+3));
          ldcwords+= 4;
          for(int i = 0; i < 40; i++) DualSampaReset( &(ds[i]) );
          break;
        }

        // Decode GBT word
        //      printf("Igt %d gbtWords %d\n",igbt,gbtWords);
        //printf("%04d: %.8X %.8X %.8X %.8X",igbt,
        //    bufpt[0], bufpt[1], bufpt[2], bufpt[3]);
        //printf(" - " BYTE_TO_BINARY_PATTERN " " BYTE_TO_BINARY_PATTERN "\n",
        //  BYTE_TO_BINARY(bufpt[3]>>8), BYTE_TO_BINARY(bufpt[3]));
        uint32_t data2bits[40];
        DecodeGBTWord((uint32_t*)bufpt, data2bits);

        //for(int i = 0; i < 2; i++) {
          //printf("Add1BitOfData(%d)\n", i);
        int dsid;
        dsid=0;
        Add1BitOfData( data2bits[dsid]&0x1, &(ds[dsid]) );
        Add1BitOfData( (data2bits[dsid]>>1)&0x1, &(ds[dsid]) );
        //dsid=3;
        //Add1BitOfData( data2bits[dsid]&0x1, &(ds[dsid]) );
        //Add1BitOfData( (data2bits[dsid]>>1)&0x1, &(ds[dsid]) );
        //}


        // Next GBT word
        igbt++;
        bufpt+= 4;
        ldcwords+= 4;



      } // end of GBT words loop

      //    if (1/*gPrintLevel >= 2*/) printf("Sync words found %d\n",nSync);
      //        if (0/*gPrintLevel >= 2*/) printf("ibit10 %d ibit50 %d\n",ibit10,ibit50);
      //        if (0/*gPrintLevel >= 2*/) printf("data64 0x%x (%d) data10 0x%x (%d)\n",data64,data64,data10,data10);



    } // end of equipment loop
  } // end of the LDC loop

  /*
  if (1) // One window frame in a DATE event
  {
    printf("Fill Offevent\n");
    gOffEvent->fRun = fDateHeaderPtr->eventRunNb;
    int status = gOffEvent->Process();
    gOffEvent->Dump(gPrintLevel);
    if (status != -1) gOffEvent->Clear(); // Dont clear for EOF (histo fill after)
    //        gSolar->fDualSampa[0]->fStatus = synchronized;
    //          gSolar->fDualSampa[1]->fStatus = synchronized;
    for (int ids = 0; ids < gSolar->fNDualSampas; ids++)
      gSolar->fDualSampa[ids]->fStatus = notSynchronized;
    //            gSolar->fDualSampa[ids]->fStatus = synchronized;

    if (status == 1) return 0;
    if (status == -1) return -1; // EOF
  }
  */
  return eventwords;
}





void  RawEvent::DumpRawEvent()
{
  struct eventHeaderStruct* dateheader;
  dateheader = (struct eventHeaderStruct*) fRawEventPtr;
  std::cout<<std::endl<<"RAW buffer print : event "
      <<dateheader->eventId[0]
                            <<" size (bytes) "<<dateheader->eventSize <<" type "<<dateheader->eventType
                            <<" run "<<dateheader->eventRunNb<<std::endl;
  for (int i = 0; i < (dateheader->eventSize)/4; i++)
  {
    std::cout << "RAW buffer word #" << i+1 << " = "  << std::hex << *(fRawEventPtr+i) <<"\t"<< " ("
        << std::dec << *(fRawEventPtr+i) << ")"<< std::endl;
  }
  std::cout<<std::endl;
}


void  RawEvent::DumpDateHeader()
{
  //
  // Copy from the eventDump.c written by the ALICE/DAQ group
  // Print of attributes not handled
  //
  printf( "\n" );
  printf( "Size:%d (header:%d)", fDateHeaderPtr->eventSize, fDateHeaderPtr->eventHeadSize );
  printf( " " );

  printf( "Version:0x%08x", fDateHeaderPtr->eventVersion );
  printf( " " );

  printf( "Type:" );
  switch ( fDateHeaderPtr->eventType ) {
  case START_OF_RUN :       printf( "StartOfRun" ); break;
  case END_OF_RUN :         printf( "EndOfRun" ); break;
  case START_OF_RUN_FILES : printf( "StartOfRunFiles" ); break;
  case END_OF_RUN_FILES :   printf( "EndOfRunFiles" ); break;
  case START_OF_BURST :     printf( "StartOfBurst" ); break;
  case END_OF_BURST :       printf( "EndOfBurst" ); break;
  case PHYSICS_EVENT :      printf( "PhysicsEvent" ); break;
  case CALIBRATION_EVENT :  printf( "CalibrationEvent" ); break;
  case EVENT_FORMAT_ERROR : printf( "EventFormatError" ); break;
  case START_OF_DATA :      printf( "StartOfData" ); break;
  case END_OF_DATA :        printf( "EndOfData" ); break;
  case SYSTEM_SOFTWARE_TRIGGER_EVENT :
    printf( "SystemSoftwareTriggerEvent" ); break;
  case DETECTOR_SOFTWARE_TRIGGER_EVENT :
    printf( "DetectorSoftwareTriggerEvent" ); break;
  case SYNC_EVENT :
    printf( "SyncEvent" ); break;
  default :                 printf( "UNKNOWN TYPE %d 0x%08x",
      fDateHeaderPtr->eventType, fDateHeaderPtr->eventType );
  }

  printf( "\n" );

  printf( "RunNb:%d", fDateHeaderPtr->eventRunNb );
  printf( " " );

  if ( TEST_SYSTEM_ATTRIBUTE( fDateHeaderPtr->eventTypeAttribute,
      ATTR_ORBIT_BC )) {
    printf( "Period:%d Orbit:%d BunchCrossing:%d",
        EVENT_ID_GET_PERIOD( fDateHeaderPtr->eventId ),
        EVENT_ID_GET_ORBIT( fDateHeaderPtr->eventId ),
        EVENT_ID_GET_BUNCH_CROSSING( fDateHeaderPtr->eventId ) );
  } else {
    printf( "nbInRun:%d burstNb:%d nbInBurst:%d",
        EVENT_ID_GET_NB_IN_RUN( fDateHeaderPtr->eventId ),
        EVENT_ID_GET_BURST_NB( fDateHeaderPtr->eventId ),
        EVENT_ID_GET_NB_IN_BURST( fDateHeaderPtr->eventId ) );
  }
  printf( " " );

  printf( "ldcId:" );
  if ( fDateHeaderPtr->eventLdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", fDateHeaderPtr->eventLdcId );
  printf( " " );

  printf( "gdcId:" );
  if ( fDateHeaderPtr->eventGdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", fDateHeaderPtr->eventGdcId );
  printf( " " );

  {
    time_t t = fDateHeaderPtr->eventTimestamp;

    printf( "time:%s", ctime( &t ) );
  } /* Note that ctime will add a "\n" !!! */

  printf( "Attributes: (%08x.%08x.%08x)\n",fDateHeaderPtr->eventTypeAttribute[0],
      fDateHeaderPtr->eventTypeAttribute[1],fDateHeaderPtr->eventTypeAttribute[2]);

  printf( "triggerPattern:%08x-%08x%s%s",
      fDateHeaderPtr->eventTriggerPattern[0],
      fDateHeaderPtr->eventTriggerPattern[1],
      TRIGGER_PATTERN_VALID(fDateHeaderPtr->eventTriggerPattern) ? "":"[invalid]",
          TRIGGER_PATTERN_OK(fDateHeaderPtr->eventTriggerPattern) ? "" : "{NOT OK}" );
  printf( " " );

  printf( "detectorPattern:%08x%s%s",
      fDateHeaderPtr->eventDetectorPattern[0],
      DETECTOR_PATTERN_VALID(fDateHeaderPtr->eventDetectorPattern) ?
          "" : "[invalid]",
          DETECTOR_PATTERN_OK(fDateHeaderPtr->eventDetectorPattern) ?
              "" : "{NOT OK}" );
  printf( "\n" );

}

void RawEvent::DumpDateEquipmentHeader()
{
  //
  // Copy from the eventDump.c written by the ALICE/DAQ group
  // Print of attributes not handled
  //
  printf( " - Equipment: size:%d type:%d id:%d basicElementSize:%d attributes:",
      fDateEquipmentHeaderPtr->equipmentSize,
      fDateEquipmentHeaderPtr->equipmentType,
      fDateEquipmentHeaderPtr->equipmentId,
      fDateEquipmentHeaderPtr->equipmentBasicElementSize );
  printf( "Attributes: (%08x.%08x.%08x)\n",fDateEquipmentHeaderPtr->equipmentTypeAttribute[0],
      fDateEquipmentHeaderPtr->equipmentTypeAttribute[1],fDateEquipmentHeaderPtr->equipmentTypeAttribute[2]);

}



int main(int argc, char** argv)
{
  char tstr[500];

  gPrintLevel = 0;  // Global variable defined as extern in the others .cxx files

  // DATE monitoring init
  int status;
  //    status = monitorSetDataSource(InputFile);
  status = monitorSetDataSource(argv[1]);
  if (status)
  {
    std::cerr << "ERROR : monitorSetDataSource status (hex) = " << std::hex << status
        << " " << monitorDecodeError(status) << std::endl;
    return -1;
  }
  status = monitorDeclareMp("MUON Tracking monitoring");
  if (status)
  {
    std::cerr << "ERROR : monitorDeclareMp status (hex) = " << std::hex << status
        << " " << monitorDecodeError(status) << std::endl;
    return -1;
  }

  int n = 0;
  void* ptr;
  RawEvent rawevent; rawevent.Init();
  while(true) {
    status = monitorGetEventDynamic(&ptr);
    if (status < 0) {
      std::cout<<"DATE EOF found"<<std::endl;
      free(ptr);
      break;
    }
    rawevent.Fill((int*)ptr);
    //printf("=========\n");
    n++;
  }

  for(int i = 0; i< 40; i++) {
    for(int j = 0; j < 2; j++) {
      for(int k = 0; k < 32; k++) {
	if( rawevent.ds[i].ndata[j][k] > 0 ) {
	  float ped = (float)(rawevent.ds[i].pedestal[j][k]/rawevent.ds[i].ndata[j][k]);
	  float noise = (float)sqrt( rawevent.ds[i].noise[j][k]/rawevent.ds[i].ndata[j][k] - ped*ped );
	  printf("%02d %d %02d:  %0.6f  %0.6f  %d %d\n",i, j, k, ped, noise, rawevent.ds[i].ndata[j][k], rawevent.ds[i].nclus[j][k]);
	}
      }
    }
  }
}
