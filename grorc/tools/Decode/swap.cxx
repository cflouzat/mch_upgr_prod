// $Id: swap.cxx,v 1.2 2007/04/26 13:06:37 albertobaldisseri Exp $
//
// MuTrkOnline
// Swaping data
// A: Baldisseri : Feb. 2006
//
//
static int swapneeded = 0;

typedef struct
{
    unsigned int b1:8,b2:8,b3:8,b4:8;
} RawWord;

int Swap (int* input, int nwords )
{
    RawWord *word,temp;
    word = (RawWord *) input;   
    for (int i = 0 ; i < nwords; i++)
	{
	    temp = *(((RawWord*)input)+i);   
	    word->b1 = temp.b4;
	    word->b2 = temp.b3;
	    word->b3 = temp.b2;
	    word->b4 = temp.b1;
	    word++;
	}

    return 0;
}
int SwapNeeded() {return swapneeded;}
void SetSwapFlag(int fl) {swapneeded = fl;}
