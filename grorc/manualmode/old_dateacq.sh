ACQDATE=`date -r ~/dataacq/test +%Y%m%d_%H%M%S`
cp ~/dataacq/test ~/dataacq/$ACQDATE'_acq'
cp ~/dataacq/test.cfg ~/dataacq/$ACQDATE'_acq.cfg'

echo "reset grorc"
grorc_reset.sh > /dev/null
usleep 6000000
HardResetON.sh > /dev/null
echo "enable SAMPAs"
HardResetOFF.sh > /dev/null
cd ../py/
rm configSampa.txt; ln -s configSampa_trg.txt configSampa.txt
python MCH_upgrade_auto.py
cd -
read -p "start date acq then press <enter> when ready"

echo "configure grorc for acq"
grorc_cont_readout.sh > /dev/null


echo "send trigger"
grorc_send_IDLE_SYNC.sh 1 > /dev/null
#grorc_send_MID.sh > /dev/null

echo "eventDump ~/dataacq/test |less"

cp ../py/configSampa_trg.txt ~/dataacq/test.cfg
