#!/usr/bin/env python

from CMch import CMch
from CConfig import CConfig

myConf=CConfig()
#(DSpos,SolarPort,GrorcPort,GrorcNb):
#myDSpos=1

#myConf.addDS(myDSpos,0,0,0)

# tetsbench @ CERN
myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(3,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')


myConf.setRocAddress(0,'5:0.0','G-RORC')
myConf.setScaLogging(True)

print(myConf)


myObj=CMch()
bStatus=myObj.configure(myConf)
if bStatus:
	myObj._configureChips()

for curGrorc in myObj._pGrorc:
	curGrorc._prepareFIFO(10000)
	myObj.sendBXCRst()
	curGrorc._openGate()
	myObj.sendTrigger()
	curGrorc._readFIFO(10)
	#curGrorc._dumpFIFO(8000,'data_FIFO.txt')
