# -*- coding: utf-8 -*-

import subprocess
from CGrorc import *
from CSca import *
from CI2c import *
from CColor import *

import random
import logging

class CSolar:
	"""SOLAR Card Management Class"""

	def __init__(self,nIdx,hdlGrorc,nI2CFreq):
		self._nVersionSolar=2
		self._bLogSca=hdlGrorc._bLogSca # when true, log commands which are sent to SCA chip
		self._nIdx=nIdx
		self._nI2CFreq=nI2CFreq
		# handles to objects
		self._hdlGrorc=hdlGrorc
		self._sca=CSca(self._hdlGrorc)

		# default setup
		self._enSPI='0'
		self._enGPIO='1'
		self._enI2C='0000000100000000' # default: read the unique number
		self._enJTAG='0'
		self._enADC='1'
		self._enDAC='0'

	def __repr__(self):
		return(str(self._hdlGrorc))

	def __str__(self):
		return(str(self._hdlGrorc.__dict__))

	def enI2CPort(self,nI2CPort):
		nIdxI2CPort=self._mapI2CPort(nI2CPort)
		# add a DS board (and also the corresponding I2C interface
		self._enI2C=self._enI2C[:15-nIdxI2CPort]+'1'+self._enI2C[16-nIdxI2CPort:]

	def setup(self):
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# *** Enable-Disable SCA ports")

		self._sca._enable(self._enSPI,self._enGPIO,self._enI2C,self._enJTAG,self._enADC,self._enDAC)
		#self._sca._configure(self._enSPI,self._enGPIO,self._enI2C,self._enJTAG,self._enADC,self._enDAC)

		nBytes=1
		#for idxI2C in range(16-1,-1,-1):
		for idxI2C in range(8-1,-1,-1):
			if '1' in self._enI2C[15-idxI2C]:
				if(self._bLogSca==True):
					HdlLog=logging.getLogger('scalog')
					HdlLog.info("# * Configure I2C speed for I2C channel %d"%(idxI2C))

				self._sca._i2c[idxI2C]._config(self._nI2CFreq,nBytes) # configure without remapping once again

		#for  the chip giving the unique id
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# * Configure I2C speed for I2C channel dedicated to Unique ID")

		nFreq=2
		nBytes=1
		self.i2c(8)._config(nFreq,nBytes)

	def i2c(self,nI2CPort):
		return(self._sca._i2c[self._mapI2CPort(nI2CPort)])

	def _mapI2CPort(self,nI2CPort):
		
		if(self._nVersionSolar==2):
			return(nI2CPort)
		else: # for solar v1
			if nI2CPort<0:
				return(nI2CPort)
			elif nI2CPort==5: #SDA00 connector 5
				return(0)
			elif nI2CPort==4: #SDA01 connector 4
				return(1)
			elif nI2CPort==3: #SDA02 connector 3
				return(2)
			elif nI2CPort==2: #SDA03 connector 2
				return(3)
			elif nI2CPort==6: #SDA04 connector 6
				return(4)
			elif nI2CPort==1: #SDA05 connector 1
				return(5)
			elif nI2CPort==7: #SDA06 connector 7
				return(6)
			elif nI2CPort==0: #SDA07 connector 0
				return(7)
			else:
				return(nI2CPort)

	def readUniqueId(self):
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# *** Read Unique ID (I2C chip)")

		# bytes read
		# address 	| data
		# 00		| device family code 0x70
		# 01		| number bits 0 to 7
		# 02		| ........... 8 to 15
		# ... 		|	...
		# 06		| number bits 40 to 47
		# 07		| CRC
		# 09		| control register
		nChipI2cPort=8 # chip is always on I2C port 8
		nChipI2CAddr=int('50',16) # chip I2C address on bus is 0x50

		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# ** Check I2C chip identifier (shall be 0x70)")
			HdlLog.info("# * Set memory pointer to address 0x0")

		# read chip identifier (shall be 0x70
		# set address 00 for link 8
		strAns=self.i2c(nChipI2cPort)._byteWr(nChipI2CAddr,0)
		if "04" not in strAns:
			print('%sERROR%s: I2C not reachable!'%(CColor.BOLD,CColor.END))
			return('')
		strChipIDRead=self.i2c(nChipI2cPort)._byteRd(nChipI2CAddr) # read the byte
		if strChipIDRead!='70':
			print('%sERROR%s: chip DS28CM00R could not be found!'%(CColor.BOLD,CColor.END))

		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# ** Read unique ID itsef")
			HdlLog.info("# * Set memory pointer to address 0x1")

		# read chip unique ID
		# set address 01 for link 8
		self.i2c(nChipI2cPort)._byteWr(nChipI2CAddr,1)
		return(self.i2c(nChipI2cPort)._mbyteRd(nChipI2CAddr,6)) # read the 6 bytes for the unique ID

