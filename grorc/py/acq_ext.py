#!/usr/bin/env python

import os
import os.path
import sys

from CMch import CMch
from CConfig import CConfig

if os.path.isfile("/home/data/data_FIFO.txt"):
        os.remove("/home/data/data_FIFO.txt")

myConf=CConfig()

# port 6
#myConf.addDS(0,6,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(0,6,0,0,'configSampa_slat_sampav3.txt','configSampa_slat.txt')
#myConf.addDS(0,6,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(1,6,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(1,6,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(2,6,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(2,6,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(3,6,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(3,6,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(4,6,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(4,6,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

# port 7
myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(0,7,0,0,'configSampa_slat_sampav3.txt','configSampa_slat.txt')
#myConf.addDS(0,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(1,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(1,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(2,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(2,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

myConf.addDS(3,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(3,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(4,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(4,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')


myConf.setRocAddress(0,'5:0.0','G-RORC')
myConf.setScaLogging(True)

FIFOsz = int(sys.argv[2])

print(myConf)

myObj=CMch()
bStatus=myObj.configure(myConf)

# Set busy flag and reset RORC
for curGrorc in myObj._pGrorc:
        curGrorc._setBusy(1)
        curGrorc._reset()

# Configure the DS boards
if bStatus:
	myObj._configureChips()

for curGrorc in myObj._pGrorc:
        #curGrorc._setBusy(1)
        #curGrorc._reset()
        curGrorc._prepareFIFO(FIFOsz)
        myObj.sendBigBXCRstExtTrig()
        myObj.sendBigTriggerExtTrig()
        #curGrorc._openGateExt()
        #curGrorc._readFIFOasync(106,int(sys.argv[1]))
        curGrorc._setBusy(0)
        #curGrorc._readFIFOasync(8006,int(sys.argv[1]))
        curGrorc._dumpFIFOasync(FIFOsz+6,int(sys.argv[1]),'/home/data/data_FIFO.txt')

