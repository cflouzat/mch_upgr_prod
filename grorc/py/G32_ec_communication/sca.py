import sys

import SCA 
from SCA import Sca


def showOption() :
    print ('-------------------')
    print('1) INIT SCA')
    print('2) TPC config')
    print('20) ENABLE GPIO')
    print('21) Push DATA to GPIO')
    print('22) GPIO INTENABLE')
    print('23) GPIO INTSEL')
    print('40) ENABLE ADC ch')
    print('41) SCA ID')
    print('Else) QUIT')
    print ('-------------------')
    
def main():
    id_card = sys.argv[1]
    gbt_ch = sys.argv[2]
    gbt_ch = int(gbt_ch)    
    board = sys.argv[3]
    
    if len(sys.argv) == 5 : 
        debug = sys.argv[4]
    else :
        debug = None
        
    sca = Sca(id_card, 2, gbt_ch, board, debug)

    sca.displayAdd()

    prev_choice = ''

    while True:
        showOption()
        try : 
            choice = input('Enter a choice : ')
        except : 
            choice = prev_choice

        if choice == 1 :
            sca.init(debug)
        elif choice == 2 :
            try : 
                tpc_file = raw_input('TPC cfg file [tpc_cmds]: ')
            except:
                tpc_file = tpc_cmds
                
            sca.TpcCfgFile(tpc_file)
            sca.TPCEN(debug)
        elif choice == 20 :
            sca.gpioEn(debug)
        elif choice == 21 :
            data = input('DATA to GPIO : ')
            sca.gpioWr(data, debug)
        elif choice == 22 :
            data = input('DATA to GPIO [0/1]: ')
            sca.gpioINTENABLE(ch, data)
        elif choice == 23 :
            data = input('DATA to GPIO : ')
            gpioINTSEL(ch, data)
        elif choice == 40 :
            adcEn(ch)
        elif choice == 41 :
            scaID(ch)
        else:
            sys.exit()
            
        prev_choice = choice
    
if __name__ == '__main__' :
    main()
