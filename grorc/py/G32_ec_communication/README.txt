# cd dev/pippo
# git clone ssh://git@gitlab.cern.ch:7999/costaf/cru-tests.git
# cp dev/pippo/cru-tests/G32-ec-communinication/sw/*.py .

git show

commit a0252a7d1e05a8a41dcbcd655bbb0ba980f6d457
Author: Filippo Costa <filippo.costa@cern.ch>
Date:   Wed Nov 1 18:19:08 2017 +0100

    renamed

diff --git a/S31-i2c-test/sw/si5338_i2c.py b/S31-i2c-test/sw/si5338_i2c.py
index 68d15b7..1cb3f5c 100755
--- a/S31-i2c-test/sw/si5338_i2c.py
+++ b/S31-i2c-test/sw/si5338_i2c.py
@@ -3,8 +3,8 @@ import sys
 import time
 from time import sleep

-import si5338_mod
-from si5338_mod import Si5338
+import SI5338
+from SI5338 import Si5338

 def showMenu():
     print ('-------------------')

