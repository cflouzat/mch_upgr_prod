# -*- coding: utf-8 -*-

import subprocess
import time
import datetime
import logging

#import sca_mod
#from sca_mod import *

from G32_ec_communication import Sca

class CGrorc:
	"""G-RORC Card Management Class"""

	def __init__(self,nIdx,strPCIeID,strType,bLogSca=False):
		self._nIdx=nIdx
		self._bLogSca=bLogSca # when true, log commands which are sent to SCA chip
		self._strType=strType
		self._bStatus="OK"
		self._strAns='empty'
		self._strPCIeID=strPCIeID
		self._hdl0=Sca(self._strPCIeID, 0, self._nIdx, self._strType)
		self._hdl2=Sca(self._strPCIeID, 2, self._nIdx, self._strType)
		self._hdlSca=self._hdl2

		# setup logger
		if(self._bLogSca==True):
			log_setup=logging.getLogger('scalog')
			fileHandler = logging.FileHandler('mch_sca_cmds.log', mode='w')
			formatter=logging.Formatter('%(message)s')
			fileHandler.setFormatter(formatter)
			log_setup.setLevel(logging.INFO)
			log_setup.addHandler(fileHandler)
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# %s"%(str(datetime.datetime.now())))

			# direct output on screen
			#streamHandler = logging.StreamHandler()
			#streamHandler.setFormatter(formatter)
			#log_setup.addHandler(streamHandler)

		#self._configure()
		self._init()
	def __repr__(self):
		return(self._bStatus)
	
	def __str__(self):
		return(self._strAns)
	
	def register_write(self,add,data):
		#self._hdl.register_write(add,data)
		#self._hdlSca.rocWr(self._hdlSca.base_add+(add-0x30),data)  # 2018/01/26 for new G-RORC firmware
		self._hdlSca.rocWr(self._hdlSca.base_add+(add-0x1c0),data)

	def _init(self,bfull=0):
		# init registers
		# these registers are not cleaned after a RESET
		#self.register_write(0x4, 0x0)  # 2018/01/26 for new G-RORC firmware
		#self.register_write(0x198, 0x0)
		#self.register_write(0x8, 0x0)
		#self.register_write(0xc, 0x0)
		self.register_write(0x194, 0x0)
		self.register_write(0x198, 0x0)
		self.register_write(0xa0, 0x0)
		self.register_write(0xa4, 0x0)
	
		# DISABLE EXT TRG bit25 reg 0a8
		# The G-RORC will not detect ext trigger (useful if there is no LVDS cable attached)
		self.register_write(0x0a8, 0x0000000)
	
		# PCIE and GBT reset
		# [0] RESET to the different logic
		# [1] GBT RESET
		if bfull==0  :
			self.register_write(0x0, 0x1)
		else :
			self.register_write(0x0, 0x3)
			# wait for the link to recover
			time.sleep(6)

	def _reset(self):
                self.register_write(0x0, 0x1)
                print"Reset G-RORC"

	def _prepareFIFO(self,nNbWordPerEvent):
		# ENABLE FIFO READOUT
		self.register_write(0x104, 0x2)
		
		# READOUT MODE CONT
		#self.register_write(0xc, 0x1)  # 2018/01/26 for new G-RORC firmware
		self.register_write(0xa4, 0x1)
		
		# CFG register
		#   0     - SEND WIDE
		#   1     - TX_POL
		#   3-2   - TX SEL
		#   8     - RX ENCODING
		#   9     - TX ENCODING
		#   19-16 - ev to collect
		#self.register_write(0x4, 0x10005)  # 2018/01/26 for new G-RORC firmware
		self.register_write(0x194, 0x10005)
		
		# how many words per event (chopping in cont readout)
		self.register_write(0x18c, nNbWordPerEvent)
		
		# OPEN THE GBT you want to read
		#self.register_write(0x8, 0x1)  # 2018/01/26 for new G-RORC firmware
		self.register_write(0xa0, 0x1)

	def _setBusy(self,flag):
                if flag == 1:
                        self.register_write(0x0a8, 0x01000000)
                else:
                        self.register_write(0x0a8, 0x00000000)

	def _openGate(self):
		# OPEN GATE
		# NO DELAY [19-0]
		# NO TRG EXT [25]
		# PULSE OPEN GATE [31]
		self.register_write(0x0a8, 0x82000000)
		self.register_write(0x0a8, 0x02000000)

	def _openGateExt(self):
		# OPEN GATE
		# NO DELAY [19-0]
		# PULSE OPEN GATE [31]
		self.register_write(0x0a8, 0x80000000)
		self.register_write(0x0a8, 0x00000000)

	def _getFIFOnwords(self):
                val = self._hdl0.rocRd(0x11c)
                wrptr = (val>>16) & 0x3FFF
                rdptr = val & 0x3FFF
                #print "wrptr=%d"%wrptr,"  rdptr=%d"%rdptr
                diff = wrptr - rdptr
                if diff < 0 :
                        diff += (0x3FFF + 1)
                return(diff)

	def _printFIFOnwords(self):
                nwords = self._getFIFOnwords()
                print "Words in FIFO: %d"%nwords

	def _dumpFIFO(self,nNbWords,strDumpFile):

		hdlfifodumpfile=open(strDumpFile, 'awb')	
		#hdlfifodumpfile=open(strDumpFile, 'wb')
	
		# ENABLE FIFO READOUT
		self._hdl2.rocWr(0x104, 0x2)
		
                while True :
                        nwords = self._getFIFOnwords()
                        print "Words in FIFO: %d"%nwords," remaining: %d"%nNbWords
                        if nwords <= 0 :
                                break
		
                        for i in range(0,nwords) :
                                self._hdl2.rocWr(0x104, 0x3)
				
                                ll = self._hdl0.rocRd(0x108)
                                lh = self._hdl0.rocRd(0x10c)
                                hl = self._hdl0.rocRd(0x110)
                                hh = self._hdl0.rocRd(0x114)
                                #    ret1 = self._hdl0.rocRd(0x118)
                                #ret2 = self._hdl0.rocRd(0x11c)
				
                                hdlfifodumpfile.write("%.8X %.8X %.8X %.8X\n"%(hh,hl,lh,ll))	
                                nNbWords -= 1
                                if nNbWords == 0 :
                                        break
                        if nNbWords == 0 :
                                break
                print "End of event"
		#self._hdl2.rocWr(0x104, 0x0)
		hdlfifodumpfile.close()

	def _readFIFO(self,nNbWords):

		# ENABLE FIFO READOUT
		self._hdl2.rocWr(0x104, 0x2)

                while True :
                        nwords = self._getFIFOnwords()
                        print "Words in FIFO: %d"%nwords
                        if nwords <= 0 :
                                break
		
                        for i in range(0,nwords) :
                                self._hdl2.rocWr(0x104, 0x3)
				
                                ll = self._hdl0.rocRd(0x108)
                                lh = self._hdl0.rocRd(0x10c)
                                hl = self._hdl0.rocRd(0x110)
                                hh = self._hdl0.rocRd(0x114)
                                #    ret1 = self._hdl0.rocRd(0x118)
                                ret2 = self._hdl0.rocRd(0x11c)
				
                                print "%.8X"%ret2, "-- %.8X"%hh,"%.8X"%hl,"%.8X"%lh,"%.8X"%ll
                                nNbWords -= 1
                                if nNbWords == 0 :
                                        break
                        if nNbWords == 0 :
                                break
		#self._hdl2.rocWr(0x104, 0x0)


	def _dumpFIFOasync(self,nNbWords,nEvents,strDumpFile):

		hdlfifodumpfile=open(strDumpFile, 'awb')	
		#hdlfifodumpfile=open(strDumpFile, 'wb')

		# ENABLE FIFO READOUT
		self._hdl2.rocWr(0x104, 0x2)

                for e in range(1,nEvents+1) :
                        nread = 0
                        while True :
                                nwords = self._getFIFOnwords()
                                #print "Words in FIFO: %d"%nwords
                                if nwords <= 0 :
                                        #print "FIFO empty, waiting for next event..."
                                        while nwords <= 0 :
                                                nwords = self._getFIFOnwords()
                                        #print "Words in FIFO: %d"%nwords

                                for i in range(0,nwords) :
                                        self._hdl2.rocWr(0x104, 0x3)
                                        
                                        ll = self._hdl0.rocRd(0x108)
                                        lh = self._hdl0.rocRd(0x10c)
                                        hl = self._hdl0.rocRd(0x110)
                                        hh = self._hdl0.rocRd(0x114)
                                        #    ret1 = self._hdl0.rocRd(0x118)
                                        #ret2 = self._hdl0.rocRd(0x11c)
                                        
                                        hdlfifodumpfile.write("%.8X %.8X %.8X %.8X\n"%(hh,hl,lh,ll))	
                                        nread += 1
                                        if nNbWords == nread :
                                                break
                                if nNbWords == nread :
                                        break
                        if (e%10) == 0:
                                print"%d"%e
		#self._hdl2.rocWr(0x104, 0x0)


	def _readFIFOasync(self,nNbWords,nEvents):

		# ENABLE FIFO READOUT
		self._hdl2.rocWr(0x104, 0x2)

                for e in range(0,nEvents) :
                        nread = 0
                        while True :
                                nwords = self._getFIFOnwords()
                                print "Words in FIFO: %d"%nwords
                                if nwords <= 0 :
                                        print "FIFO empty, waiting for next event..."
                                        while nwords <= 0 :
                                                nwords = self._getFIFOnwords()
                                        print "Words in FIFO: %d"%nwords

                                for i in range(0,nwords) :
                                        self._hdl2.rocWr(0x104, 0x3)
                                        
                                        ll = self._hdl0.rocRd(0x108)
                                        lh = self._hdl0.rocRd(0x10c)
                                        hl = self._hdl0.rocRd(0x110)
                                        hh = self._hdl0.rocRd(0x114)
                                        #    ret1 = self._hdl0.rocRd(0x118)
                                        ret2 = self._hdl0.rocRd(0x11c)
                                        
                                        print "%.8X"%ret2, "-- %.8X"%hh,"%.8X"%hl,"%.8X"%lh,"%.8X"%ll
                                        nread += 1
                                        if nNbWords == nread :
                                                break
                                if nNbWords == nread :
                                        break
		#self._hdl2.rocWr(0x104, 0x0)


	def _configure(self):
		self._strAns=subprocess.Popen("../scripts/GRORC_init.sh %s"%(self._strPCIeID),shell=True,stdout=subprocess.PIPE).stdout.read()
		if 'Config DONE' in self._strAns:
			#if '0000 11ff' in self._strAns:
				self._bStatus='OK'
			#else:
			#	self._bStatus="ERROR: Not locked"
			#	print(self._bStatus)
		else:
			self._bStatus="ERROR: G-RORC firmware problem"
		self._init(1)

	def _initSca(self):
		return
		if(self._bLogSca==True):
			HdlLog=logging.getLogger('scalog')
			HdlLog.info("# Init SCA")
			#HdlLog.info("# Disabled Init SCA")

		#print('SCA init disabled for the moment')
		self._hdlSca.init()
		#scaInit(self._hdl)

	def _write(self,strVal1,strVal2):
		#print("scaWr(self._hdl,0x%s,0x%s)"%(strVal1,strVal2))
		self._hdlSca.wr(int(strVal1,16),int(strVal2,16))
		#scaWr(self._hdl,int(strVal1,16),int(strVal2,16))

	def _read(self):
		self._strAns=self._hdlSca.rd()
		#self._strAns=scaRd(self._hdl)
		return(self._strAns)

#	def _exec(self):
#		self._strAns=subprocess.Popen("../scripts/gbt_sca_exe.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
