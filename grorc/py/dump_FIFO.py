#!/usr/bin/env python

import os
import os.path
import sys

from CMch import CMch
from CConfig import CConfig

if os.path.isfile("data_FIFO.txt"):
        os.remove("data_FIFO.txt")

myConf=CConfig()
#myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(0,7,0,0,'configSampa_slat_sampav3.txt','configSampa_slat.txt')
#myConf.addDS(0,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

myConf.addDS(1,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(1,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(2,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(2,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(3,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(3,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')

#myConf.addDS(4,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(4,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')


myConf.setRocAddress(0,'5:0.0','G-RORC')
myConf.setScaLogging(True)

print(myConf)

myObj=CMch()
bStatus=myObj.configure(myConf)

# Set busy flag and reset RORC
for curGrorc in myObj._pGrorc:
        curGrorc._setBusy(1)
        curGrorc._reset()

# Configure the DS boards
#if bStatus:
#	myObj._configureChips()

for curGrorc in myObj._pGrorc:
        #curGrorc._setBusy(1)
        #curGrorc._reset()
        curGrorc._prepareFIFO(80)
        #myObj.sendBigBXCRstExtTrig()
        myObj.sendBigTriggerExtTrig()
        curGrorc._setBusy(0)
        curGrorc._readFIFOasync(86,int(sys.argv[1]))
        #curGrorc._readFIFOasync(86,int(sys.argv[1]))
        #curGrorc._dumpFIFOasync(8006,int(sys.argv[1]),'data_FIFO.txt')

