#! /bin/bash

cp -f 1_nozerosupr_noclusum_slat.txt configSampa_slat.txt
cp -f 1_nozerosupr_noclusum_slat_sampav3.txt configSampa_slat_sampav3.txt

stop=0
PID=0

trap "stop=1; echo \"Signal trapped\"" SIGINT SIGTERM

rm -f acq_test*.log /home/data/acq_test-*.txt
for i in $(seq 1 $2); do
    echo "" | tee -a acq_test.log
    echo "======================" | tee -a acq_test.log
    echo "Starting cycle $i" | tee -a acq_test.log
    echo "" | tee -a acq_test.log
    #./acq_auto.py $1
    ./acq_ext.py $1 8000 & #>> acq_test.log &
    PID=$!
    echo "Waiting completion of $PID"
    wait $PID
    WPID=$!
    echo "Wait exit code: $WPID"
    if [ $WPID -gt 128 ]; then
	#kill -TERM $PID
	echo "Waiting termination of $PID"
	#wait $PID
    fi
    

    #../tools/Decode/SOLARdecode data_FIFO.txt | less
    echo -n "Decoding file..."
    ../tools/Decode/FILEdecode -d 0 /home/data/data_FIFO.txt > acq_test-tmp.log &
    PID=$!
    wait $PID
    wait $PID
    echo " decoding finished"
    test=$(cat acq_test-tmp.log | grep "ERRORS: 00000" | grep "WARNINGS: 00000")
    if [ -z "$test" ]; then
	id=$(printf "%03d" $i)
	echo "Run $id contains errors, saving the data file..."
	cp /home/data/data_FIFO.txt /home/data/acq_test-${id}.txt
    fi
    cat acq_test-tmp.log >> acq_test.log
    echo "" >> acq_test.log
    echo "############################################" >> acq_test.log
    echo "" >> acq_test.log

    if [ x"$stop" = "x1" ]; then
	break
    fi
done

rm -f acq_test-tmp.log

echo -n "Do you want to save this run [y/N]? "
read answer
if [ x"$answer" = "xy" ]; then
    now=$(date +%Y%m%d-%H%M%S)
    rm -rf /home/data/acq_test-${now}
    mkdir -p /home/data/acq_test-${now}
    mv /home/data/acq_test-*.txt acq_test.log /home/data/acq_test-${now}
    gzip /home/data/acq_test-${now}/acq_test.log
fi
