#!/usr/bin/env python

import os
import os.path
import sys

from CMch import CMch
from CConfig import CConfig

myConf=CConfig()
myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(3,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(3,7,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')


myConf.setRocAddress(0,'5:0.0','G-RORC')
myConf.setScaLogging(True)

print(myConf)

myObj=CMch()
bStatus=myObj.configure(myConf)
if bStatus:
	myObj._configureChips()

if os.path.isfile("data_FIFO.txt"):
        os.remove("data_FIFO.txt")

for x in range(0, int(sys.argv[1])):
        for curGrorc in myObj._pGrorc:
                curGrorc._prepareFIFO(10000)
                myObj.sendBigBXCRst()
                curGrorc._openGate()
                myObj.sendBigTrigger()
                #curGrorc._readFIFO(106)
                curGrorc._dumpFIFO(10006,'data_FIFO.txt')

