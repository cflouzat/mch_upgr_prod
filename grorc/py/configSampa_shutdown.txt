# configuration applied to all sampa chips
# address data
# you can add comment followed by #
# even at the end of a line
# by default values are interpreted as decimal values
# but a 'h or 'b prefix allows to write hex or binary values
# examples:
# 'haa 'b0001
# 'b101 14 # you can add a comment here
# empty lines have no incidence
# lines beginning with a # are not seen
 
#"QUIET mode for sampa chip



'h0e 'h5 #software reset at the beginning

'h0d 'h20 # trigger mode et power save
'h12 'h11 # disable 100 ohm and only 1 link
'h07 'h09 # TWLEN 63
'h08 'h00 # TWLEN 63
'h0b 'h08 # ACQEND 62
'h0c 'h00 # ACQEND 62

'h06 192 # pre trigger delay set to 19.2 us

#'h13 'h00 # set output power to norm
#'h13 'h55 # set output power to low
'h13 'haa # set output power to max!
'h24 'hff
'h25 'hff
'h26 'hff
'h27 'hff

'h1f 'h3

'h0e 'h5 #software reset at the end
