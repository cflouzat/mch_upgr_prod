# configuration applied to all sampa chips
# address data
# you can add comment followed by #
# even at the end of a line
# by default values are interpreted as decimal values
# but a 'h or 'b prefix allows to write hex or binary values
# examples:
# 'haa 'b0001
# 'b101 14 # you can add a comment here
# empty lines have no incidence
# lines beginning with a # are not seen
#" read address 0
 
#'h00 'h0 #software reset at the beginning
'h07 'h0 #software reset at the beginning
