#! /bin/bash

./gen_patt.sh $1 >& /dev/null
cp -f 9_nozerosupr_noclusum_patt_slat.txt configSampa_slat.txt

rm -f acq_patt.log
for i in $(seq 1 $3); do
    #./acq_auto.py $2
    ./acq_ext.py $2

    ../tools/Decode/FILEdecode -d 0 -p $1 data_FIFO.txt >> acq_patt.log
    echo "" >> acq_patt.log
    echo "############################################" >> acq_patt.log
    echo "" >> acq_patt.log
done
