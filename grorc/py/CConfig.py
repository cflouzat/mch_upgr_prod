# -*- coding: utf-8 -*-

class CConfDS:
	"""Default Configuration of a Dual-SAMPA board"""

	def __init__(self,nIdx,nPort,strConfigFileSampa0,strConfigFileSampa1):
		self.nIdx=nIdx
		self.nPort=nPort
		self.strConfigFileSampa0=strConfigFileSampa0
		self.strConfigFileSampa1=strConfigFileSampa1

	def __repr__(self):
		return('%d(%d)'%(self.nPort,self.nIdx))

	def addDS(self,DSpos,SolarPort,strConfigFileSampa0,strConfigFileSampa1):
		if(SolarPort==self.nPort):
			if(DSpos==self.nIdx):
				return(1)
		#else:
		return(0)
		
class CConfSolar:
	"""Default Configuration of a SOLAR board"""

	def __init__(self,nIdx):
		self.nIdx=nIdx
		self.pDS=[]

	def __repr__(self):
		mystring='%s['%str(self.nIdx)
		for curDS in self.pDS:
			mystring="%s %s"%(mystring,str(curDS))
		mystring="%s ]"%mystring
		return(mystring)

	def _add(self,DSpos,SolarPort,GrorcPort,strConfigFileSampa0,strConfigFileSampa1):
		for curDS in self.pDS:
			if(curDS.addDS(DSpos,SolarPort,strConfigFileSampa0,strConfigFileSampa1)==1):
				return(1)
		#if(ans==0):
		curDS=CConfDS(DSpos,SolarPort,strConfigFileSampa0,strConfigFileSampa1)
		self.pDS.append(curDS)
		return(1)

	def addDS(self,DSpos,SolarPort,GrorcPort,strConfigFileSampa0,strConfigFileSampa1):
		if(GrorcPort==self.nIdx):
			ans=self._add(DSpos,SolarPort,GrorcPort,strConfigFileSampa0,strConfigFileSampa1)
			return(ans)
		else:
			return(0)

class CConfGrorc:
	"""Default Configuration of a G-RORC board"""

	def __init__(self,nIdx):
		self.nIdx=nIdx
		self.pSolar=[]
		self.strPCIeID='2:0.0'
		self.strType='G-RORC'

	def __repr__(self):
		mystring=''
		for curSolar in self.pSolar:
			mystring="%s%s\n"%(mystring,str("%d:%s"%(self.nIdx,str(curSolar))))
		return(mystring)
	
	def _add(self,DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1):
		for curSolar in self.pSolar:
			if(curSolar.addDS(DSpos,SolarPort,GrorcPort,strConfigFileSampa0,strConfigFileSampa1)==1):
				return(1)
		#if(ans==0):
		curSolar=CConfSolar(GrorcPort)
		curSolar.addDS(DSpos,SolarPort,GrorcPort,strConfigFileSampa0,strConfigFileSampa1)
		self.pSolar.append(curSolar)
		return(1)

	def addDS(self,DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1):
		if(GrorcNb==self.nIdx):
			ans=self._add(DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1)
			return(ans)
		else:
			return(0)

class CConfig:
	"""Configuration of the Detector"""

	def __init__(self):
		self.pGrorcPCIeID=['2:0.0','5:0.0']
		self.pGrorc=[]
		self.bLogSca=False

	def setRocAddress(self,GrorcNb,strPCIeID,strType):
		for curGrorc in self.pGrorc:
                        if(curGrorc.nIdx==GrorcNb):
                                curGrorc.strPCIeID=strPCIeID
                                curGrorc.strType=strType
                                return()
	
	def setScaLogging(self,bLogSca):
		self.bLogSca=bLogSca


	def addDS(self,DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1):
		for curGrorc in self.pGrorc:
			if(curGrorc.addDS(DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1)==1):
				return()
		#if(ans==0):
		curGrorc=CConfGrorc(GrorcNb)
		curGrorc.addDS(DSpos,SolarPort,GrorcPort,GrorcNb,strConfigFileSampa0,strConfigFileSampa1)
		self.pGrorc.append(curGrorc)

	def __repr__(self):
		mystring=''
		for curGrorc in self.pGrorc:
			mystring="%s%s\n"%(mystring,str(curGrorc))
		return(mystring)
