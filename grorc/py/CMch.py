# -*- coding: utf-8 -*-

from CConfig import *

from CGrorc import *
from CSolar import *
from CSampa import *
from CColor import *

class CMch:
	"""Muon Chamber management Class"""

	def __init__(self):
		self._bLogSca=False
		self._pGrorc=[] # to grab any grorc
		self._pSolar=[] # to grab any SOLAR
		self._pSampa=[] # to grab any SAMPA chip
		self._nI2CFreq=2 # default frequency for I2C operation with sampa chips

	def help(self):
		print("myObj=CMch()")
		print "myObj.configure(myConf)"
		print "myObj._configureChips()"
		print "(nSampaNb,nDSPos,nI2CPort,nGrorcPort,nGrorcNb):"
		print "hdlSampa0=myObj.getSampa(0,0,0,0,0)"
		print "hdlSampa1=myObj.getSampa(1,0,0,0,0)"
		print "hdlSampa2=myObj.getSampa(0,1,0,0,0)"
		print "hdlSampa3=myObj.getSampa(1,1,0,0,0)"
		print "hdlSampa4=myObj.getSampa(0,2,0,0,0)"
		print "hdlSampa5=myObj.getSampa(1,2,0,0,0)"
		print "hdlSampa6=myObj.getSampa(0,3,0,0,0)"
		print "hdlSampa7=myObj.getSampa(1,3,0,0,0)"
		print "hdlSampa8=myObj.getSampa(0,4,0,0,0)"
		print "hdlSampa9=myObj.getSampa(1,4,0,0,0)"
		print "print(myObj.readSampa(hdlSampa,int('13',16)))"
		print "hdlSampa0.configure('configSampa.txt')"
		print "hdlSampa1.configure('configSampa.txt')"
		print "hdlSampa2.configure('configSampa_monitor.txt')"
		print "myObj.help() #for this help"
		
	def configure(self,config):
		self._pGrorc[:]=[] # empty grorc list
		self._pSolar[:]=[] # empty SOLAR list
		self._pSampa[:]=[] # empty SAMPA chip list
		self.config=config
		self._bLogSca=config.bLogSca
		lbStatus=True
		for curGrorc in self.config.pGrorc:
			hdlGrorc=CGrorc(curGrorc.nIdx,curGrorc.strPCIeID,curGrorc.strType,self._bLogSca)
			if 'ERROR' in hdlGrorc._bStatus:
				lbStatus=False
				print(hdlGrorc._bStatus)
			else:
				self._pGrorc.append(hdlGrorc)
				hdlGrorc._initSca()
				for curSolar in curGrorc.pSolar:
					hdlSolar=CSolar(curSolar.nIdx,hdlGrorc,self._nI2CFreq)
					self._pSolar.append(hdlSolar)
					for curDS in curSolar.pDS:
						hdlSolar.enI2CPort(curDS.nPort) # enables its I2C interface ...
						hdlSampa0=CSampa(curGrorc.nIdx,curSolar.nIdx,curDS.nPort,curDS.nIdx,0,hdlSolar,curDS.strConfigFileSampa0)
						hdlSampa1=CSampa(curGrorc.nIdx,curSolar.nIdx,curDS.nPort,curDS.nIdx,1,hdlSolar,curDS.strConfigFileSampa1)
						self._pSampa.append(hdlSampa0)
						self._pSampa.append(hdlSampa1)
					hdlSolar.setup()
					strUniqueID=hdlSolar.readUniqueId()
					if strUniqueID=='':
						print('%sERROR%s: SOLAR Card on GRORC %d - SOLAR %d could not be found!'%(CColor.BOLD,CColor.END,curGrorc.nIdx,curSolar.nIdx))
						lbStatus=False
						break
					else:
						print('SOLAR %s has unique ID 0x%s'%(curSolar.nIdx,strUniqueID))
		if(lbStatus==True):
			self.enableContReadout(1024)
		return(lbStatus)
				#self._configureChips()

	def _configureChips(self):
		self.reset()
		self.stopReset()
		#print('%sSAMPA Configuration: First attempt%s'%(CColor.BLUE,CColor.END))
		self._reconfigureChips()
		#print('%sSAMPA Configuration: Second attempt%s'%(CColor.BLUE,CColor.END))
		#self._reconfigureChips()

	def _reconfigureChips(self):
		for curSampa in self._pSampa:
			curSampa.configure()

	def enableContReadout(self,nXevent):
		for curGrorc in self._pGrorc:
			# ENABLE FIFO READOUT
			curGrorc.register_write(0x104, 0x2)
			
			# READOUT MODE CONT
			curGrorc.register_write(0xa4, 0x1)
			
			# CFG register
			#   0 		- SEND WIDE
			#   1 		- TX_POL
			#   3-2		- TX SEL
			#   8		- RX ENCODING
			#   9		- TX ENCODING
			#   19-16	- ev to collect
			curGrorc.register_write(0x194, 0xF0005)
			
			# how many words per event (curGrorcopping in cont readout)
			curGrorc.register_write(0x18c, nXevent)
			
			# OPEN THE GBT you want to read
			curGrorc.register_write(0xa0, 0x1)
	
	def sendBigBXCRst(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x60000000 + word_max
		word_STOP = 0x40000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('03C0F03C',16))
			curGrorc.register_write(0x1a4, int('C0F03C0F',16))
			curGrorc.register_write(0x1a8, int('00000F03',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def sendBigBXCRstExtTrig(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x20000000 + word_max
		word_STOP = 0x00000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('03C0F03C',16))
			curGrorc.register_write(0x1a4, int('C0F03C0F',16))
			curGrorc.register_write(0x1a8, int('00000F03',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def sendBXCRst(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x60000000 + word_max
		word_STOP = 0x40000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('0340D034',16))
			curGrorc.register_write(0x1a4, int('40D0340D',16))
			curGrorc.register_write(0x1a8, int('00000D03',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def sendBigTrigger(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x60000000 + word_max
		word_STOP = 0x40000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('0F03C0F0',16))
			curGrorc.register_write(0x1a4, int('03C0F03C',16))
			curGrorc.register_write(0x1a8, int('00003C0F',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def sendBigTriggerExtTrig(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x00000000 + word_max
		word_STOP = 0x00000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('0F03C0F0',16))
			curGrorc.register_write(0x1a4, int('03C0F03C',16))
			curGrorc.register_write(0x1a8, int('00003C0F',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def sendTrigger(self):
		#self._strAns=subprocess.Popen("../manualmode/grorc_send_MID.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		word_max=1
		
		#The mapping of the 32 bits are [TBC!]
		#s_word_max        <= s_reg(7 downto 0); -- number of cc you keep the pattern valid
		#s_delay_open_gate <= s_reg(27 downto 8); -- delay before opening the gate (step seems 4ns i.e. 250 MHz)
		#s_gbt_val         <= s_reg(28);
		#s_wr              <= s_reg(29);
		#s_open_gate       <= s_reg(30);
		#s_set_idle        <= s_reg(31);
		#/date/rorc/Linux/rorc_write_reg -m0 -c2 -a0x198 -v0x60000001
		
		word_max = word_max & 0xff
		word_START = 0x60000000 + word_max
		word_STOP = 0x40000000 + word_max

		for curGrorc in self._pGrorc:
			curGrorc.register_write(0x1a0, int('0701C070',16))
			curGrorc.register_write(0x1a4, int('01C0701C',16))
			curGrorc.register_write(0x1a8, int('00001C07',16))
			curGrorc.register_write(0x198, word_START)
			curGrorc.register_write(0x198, word_STOP)
	
	def reset(self):
		#self._strAns=subprocess.Popen("../manualmode/HardResetON.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		for curGrorc in self._pGrorc:
			# IDLE PATTERN
			curGrorc.register_write(0x1a0, int('00000000',16))
			curGrorc.register_write(0x1a4, int('00000000',16))
			curGrorc.register_write(0x1a8, int('00000000',16))
			
			# SET IT AS IDLE WORD
			curGrorc.register_write(0x198, 0x80000000)
			curGrorc.register_write(0x198, 0x0)

	
	def stopReset(self):
		#self._strAns=subprocess.Popen("../manualmode/HardResetOFF.sh",shell=True,stdout=subprocess.PIPE).stdout.read()
		for curGrorc in self._pGrorc:
			# IDLE PATTERN
			curGrorc.register_write(0x1a0, int('0300C030',16))
			curGrorc.register_write(0x1a4, int('00C0300C',16))
			curGrorc.register_write(0x1a8, int('00000C03',16))
			
			# SET IT AS IDLE WORD
			curGrorc.register_write(0x198, 0x80000000)
			curGrorc.register_write(0x198, 0x0)


	def date(self):
		subprocess.Popen("../manualmode/date.sh")
		self._strAns=subprocess.Popen("../manualmode/grorc_cont_readout.sh",shell=True,stdout=subprocess.PIPE).stdout.read()

	def getSampa(self,nSampaNb,nDSPos,nI2CPort,nGrorcPort,nGrorcNb):
		for curSampa in self._pSampa:
			if curSampa.located(nSampaNb,nDSPos,nI2CPort,nGrorcPort,nGrorcNb)==1:
				return(curSampa)
		#if ans==0:
		print 'ERROR: Sampa chip %d at pos=%d i2cport=%d solarnb=%d grorcnb=%d cound not be found'%(nSampaNb,nDSPos,nI2CPort,nGrorcPort,nGrorcNb)
		newSampa=CSampa() # empty
		return(newSampa)

	def readSampa(self,hdlSampa,nAddrReg):
		return(hdlSampa.read(nAddrReg))



