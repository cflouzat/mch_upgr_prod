#! /bin/bash

cp -f 1_nozerosupr_noclusum_slat.txt configSampa_slat.txt
cp -f 1_nozerosupr_noclusum_slat_sampav3.txt configSampa_slat_sampav3.txt

stop=0
PID=0

trap "stop=1; echo \"Signal trapped\"" SIGINT SIGTERM

rm -f acq_ped*.log acq_ped-*.txt
    echo "" | tee -a acq_ped.log
    echo "======================" | tee -a acq_ped.log
    echo "Starting cycle $i" | tee -a acq_ped.log
    echo "" | tee -a acq_ped.log
    #./acq_auto.py $1
    ./acq_ext.py $1 8000 & #>> acq_ped.log &
    PID=$!
    echo "Waiting completion of $PID"
    wait $PID
    WPID=$!
    echo "Wait exit code: $WPID"
    if [ $WPID -gt 128 ]; then
	kill -TERM $PID
	echo "Waiting termination of $PID"
	wait $PID
    fi
    

    #../tools/Decode/SOLARdecode data_FIFO.txt | less
    ../tools/Decode/FILEdecode -d 0 -n 1 /home/data/data_FIFO.txt > acq_ped-tmp.log &
    PID=$!
    wait $PID
    wait $PID
    ../tools/Decode/TXT2DATE /home/data/data_FIFO.txt &
    PID=$!
    wait $PID
    wait $PID
    cat acq_ped-tmp.log >> acq_ped.log
    echo "" >> acq_ped.log
    echo "############################################" >> acq_ped.log
    echo "" >> acq_ped.log


rm -f acq_ped-tmp.log

echo -n "Do you want to save this run [y/N]? "
read answer
if [ x"$answer" = "xy" ]; then
    now=$(date +%Y%m%d-%H%M%S)
    rm -rf /home/data/acq_ped-${now}
    mkdir -p /home/data/acq_ped-${now}
    mv acq_ped.log /home/data/acq_ped-${now}
    gzip -c /home/data/data_FIFO.txt > /home/data/acq_ped-${now}/data_FIFO.txt.gz
    cp /home/data/out.dat /home/data/acq_ped-${now}
    gzip /home/data/acq_ped-${now}/acq_ped.log
fi
