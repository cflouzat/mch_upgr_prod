#! /bin/bash

patt=$1

cp 9_nozerosupr_noclusum_patt_slat_head.txt 9_nozerosupr_noclusum_patt_slat.txt

echo "# hit pattern" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h17 'h10" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h15 0" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h16 0" >> 9_nozerosupr_noclusum_patt_slat.txt

echo "" >> 9_nozerosupr_noclusum_patt_slat.txt


echo "# Sample #0" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h18 'h${patt}" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h19 'h${patt}" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h1a 'b01100000" >> 9_nozerosupr_noclusum_patt_slat.txt

echo "" >> 9_nozerosupr_noclusum_patt_slat.txt

i=1

while [ $i -lt 1024 ]; do
  pmadd=$(printf "%03X" $i)
  pmaddl=$(echo -n $pmadd | tail -c 2)
  pmaddh=$(echo	-n $pmadd | head	-c 1)
  echo "pmadd = 0x$pmadd  L=0x$pmaddl  H=$pmaddh"
  echo "# Sample #${i}" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "'h15 'h$pmaddl" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "'h16 'h$pmaddh" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "'h18 'h${patt}" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "'h19 'h${patt}" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "'h1a 'b01100000" >> 9_nozerosupr_noclusum_patt_slat.txt
  echo "" >> 9_nozerosupr_noclusum_patt_slat.txt
  i=$((i+1))
done

echo "# enable reading of pedestal memory" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h17 'h18" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h18 'h0A" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h19 0" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h1a 'b01100000" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "" >> 9_nozerosupr_noclusum_patt_slat.txt


echo "# zero FPD" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h17 'h0C" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h18 0" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h19 0" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "'h1a 'b01100000" >> 9_nozerosupr_noclusum_patt_slat.txt
echo "" >> 9_nozerosupr_noclusum_patt_slat.txt

echo "'h0e 'h5 #software reset at the end" >> 9_nozerosupr_noclusum_patt_slat.txt

