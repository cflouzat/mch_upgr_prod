#! /bin/bash

#cp -f 1_nozerosupr_noclusum_slat.txt configSampa_slat.txt
cp -f 5_zerosupr_noclusum_5us_10uswindow_slat.txt configSampa_slat.txt
cp -f 1_nozerosupr_noclusum_slat_sampav3.txt configSampa_slat_sampav3.txt

stop=0
PID=0

trap "stop=1; echo \"Signal trapped\"" SIGINT SIGTERM

rm -f acq_phys*.log acq_phys-*.txt
    echo "" | tee -a acq_phys.log
    echo "======================" | tee -a acq_phys.log
    echo "Starting cycle $i" | tee -a acq_phys.log
    echo "" | tee -a acq_phys.log
    #./acq_auto.py $1
    ./acq_ext.py $1 4000 & #>> acq_phys.log &
    PID=$!
    echo "Waiting completion of $PID"
    wait $PID
    WPID=$!
    echo "Wait exit code: $WPID"
    if [ $WPID -gt 128 ]; then
	kill -TERM $PID
	echo "Waiting termination of $PID"
	wait $PID
    fi
    

    #../tools/Decode/SOLARdecode data_FIFO.txt | less
    ../tools/Decode/FILEdecode -d 0 /home/data/data_FIFO.txt > acq_phys-tmp.log &
    PID=$!
    wait $PID
    wait $PID
    ../tools/Decode/TXT2DATE /home/data/data_FIFO.txt &
    PID=$!
    wait $PID
    wait $PID
    cat acq_phys-tmp.log >> acq_phys.log
    echo "" >> acq_phys.log
    echo "############################################" >> acq_phys.log
    echo "" >> acq_phys.log


rm -f acq_phys-tmp.log

echo -n "Do you want to save this run [y/N]? "
read answer
if [ x"$answer" = "xy" ]; then
    now=$(date +%Y%m%d-%H%M%S)
    rm -rf /home/data/acq_phys-${now}
    mkdir -p /home/data/acq_phys-${now}
    cp /home/data/out.dat /home/data/acq_phys-${now}
    gzip -c /home/data/data_FIFO.txt > /home/data/acq_phys-${now}/data_FIFO.txt.gz
    wd=$(pwd)
    cd ../tools/mutrk
    echo -n "Processing data file..."
    ./mutrkmch.exe -d 0 >& "${wd}/acq_phys.log"
    echo " finished."
    root -b -q plot_slat.C
    cp out.dat.root out.dat.pdf "/home/data/acq_phys-${now}"
    gzip -c "${wd}/acq_phys.log" > "/home/data/acq_phys-${now}/acq_phys.log.gz"
fi
