# -*- coding: utf-8 -*-
import mmap
class CReader:
	def __init__(self):
		self.idx=0
		self.strData=''
		self.strCurWord=''
		self.elinks=''
		self.elink=[]
		for idx in range (0,40):
			self.elink.append('')

	def _addWord(self,word):
		if len(word)>=8 :
			self.idx=self.idx+1
			self.strCurWord=self.strCurWord+word
			if self.idx>=3:
				#print(self.strCurWord)
				lelinks=''
				for idx in range(0,40/2):
					strbits="{0:04b}".format(int(self.strCurWord[24-idx-1], 16))
					#if(idx==0):
					#	print('%s %s %s %s'%(self.strCurWord[24-idx-1], strbits, str(int(strbits[:2],2)), str(int(strbits[2:],2))))
					self.elink[2*idx]  =self.elink[2*idx]  +str(int(strbits[2:],2))+'\n'
					self.elink[2*idx+1]=self.elink[2*idx+1]+str(int(strbits[:2],2))+'\n'
					lelinks=str(int(strbits[:2],2))+' '+str(int(strbits[2:],2))+' '+lelinks
				self.elinks=self.elinks + self.strCurWord+' | '+ lelinks +'\n'
				self.strData=self.strCurWord+'\n'
				self.strCurWord=''
				self.idx=0

	def add(self,string):
		strList=string.split(' ')
		for word in strList:
			if '|' in word:
				return
			self._addWord(word)

dataRead=CReader()

#self._strAns=subprocess.Popen("./GRORC_read_tmp.sh",shell=True,stdout=subprocess.PIPE).stdout.read()

a=0 # nb lines to read at beginning
with open('test.txt','r') as f:
	fmap=mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
	while(a<12):
		a=a+1
		idx=fmap.find('Size')
		if idx==-1 :
			print('Nothing found')
			break
		else:
			fmap.seek(idx)
		idxend=fmap.find(' ')
		framesz=int(fmap[idx+5:idxend])
		strLine=fmap.readline()
		if framesz==98424:
		#if framesz>80:
			# forget the 4 first lines
			strLine=fmap.readline()
			strLine=fmap.readline()
			strLine=fmap.readline()
			strLine=fmap.readline()
			
			idx=fmap.find(') ')
			# drop first 2x 32 bits
			idx=fmap.find(' ',idx+2)
			idx=fmap.find(' ',idx+1)
			fmap.seek(idx+1)
		
			strLine=fmap.readline()
			while(strLine!='') :
				#while(not strLine.startswith('.')) :
				dataRead.add(strLine)
				idx=fmap.tell()
				if(fmap[idx]=='.'):
					break
				idx=fmap.find(') ')
				fmap.seek(idx+2)
				strLine=fmap.readline()
			break
			
	fmap.close()
	#print('coucou')
	print(dataRead.elinks)
	#print(dataRead.strData)


print("mystr=dataRead.elink[0].replace('\n','')")
