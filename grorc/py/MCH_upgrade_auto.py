#!/usr/bin/env python

from CMch import CMch
from CConfig import CConfig

myConf=CConfig()
#(DSpos,SolarPort,GrorcPort,GrorcNb):
#myDSpos=1

#myConf.addDS(myDSpos,0,0,0)

# tetsbench @ Saclay
#myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(1,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')

# test flex at Saclay
#myConf.addDS(0,7,0,0,'configSampa_prbs.txt','configSampa_prbs.txt')
#myConf.addDS(1,7,0,0,'configSampa_prbs.txt','configSampa_prbs.txt')
#myConf.addDS(2,7,0,0,'configSampa_prbs.txt','configSampa_prbs.txt')
#myConf.addDS(3,7,0,0,'configSampa_prbs.txt','configSampa_prbs.txt')
#myConf.addDS(4,7,0,0,'configSampa_prbs.txt','configSampa_prbs.txt')
myConf.addDS(0,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(1,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(2,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(3,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(4,7,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(0,5,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(1,5,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(2,5,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(3,5,0,0,'configSampa_slat.txt','configSampa_slat.txt')
myConf.addDS(4,5,0,0,'configSampa_slat.txt','configSampa_slat.txt')

#SLAT
#myConf.addDS(0,0,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(1,0,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(2,0,0,0,'configSampa_slat.txt','configSampa_slat.txt')
#myConf.addDS(3,0,0,0,'configSampa_shutdown.txt','configSampa_shutdown.txt')
#myConf.addDS(4,0,0,0,'configSampa_slat_0.txt','configSampa_slat_1.txt')

# QUADRANT
# myConf.addDS(0,1,0,0,'configSampa_quad_0.txt','configSampa_quad_1.txt')
# myConf.addDS(1,1,0,0,'configSampa_quad_0.txt','configSampa_quad_1.txt')
# myConf.addDS(2,1,0,0,'configSampa_quad_0.txt','configSampa_quad_1.txt')
# myConf.addDS(3,1,0,0,'configSampa_quad_0.txt','configSampa_quad_1.txt')
# myConf.addDS(4,1,0,0,'configSampa_quad_0.txt','configSampa_quad_1.txt')

myConf.setRocAddress(0,'2:0.0','G-RORC')
#myConf.setRocAddress(1,'5:0.0','CRU')
myConf.setScaLogging(True)

print(myConf)


myObj=CMch()
bStatus=myObj.configure(myConf)
if bStatus:
	myObj._configureChips()

for curGrorc in myObj._pGrorc:
	curGrorc._prepareFIFO(10000)
	myObj.sendBXCRst()
	curGrorc._openGate()
	myObj.sendTrigger()
	curGrorc._readFIFO(10)
	#curGrorc._dumpFIFO(8000,'data_FIFO.txt')
