##########################################################################################
#
# File        : speed_change_link.tcl 
#
# Author      : Filippo Costa (filippo.costa at cern.ch)
#
# Description : script to change the link speed of the C-RORC
#               how the script should be executed @ P2
#               TPC RCU1 @ 2.125: tclsh speed_change_link.tcl TPC1 0 
#               TPC RCU2 @ 3.125: tclsh speed_change_link.tcl TPC2 0 1
#               TRD GTU @ 4.0   : tclsh speed_change_link.tcl TRD 0 1
#               HLT SIU @ 5.3125: tclsh speed_change_link.tcl HLT 0 0
#               CTP SIU @ 2.125 : tclsh speed_change_link.tcl CTP 0 0
#
# Revision    :
#               v 1.0 PC created
#               v 1.1 PC added check to the link ... if they are ok, don't execute the script
##########################################################################################

# Is it needed?
proc func_checkWatchdogQSFP {} {    
    global rd_reg g_minor
    
    after 2000
    catch {exec $rd_reg -m$g_minor -a140 | grep 140} msg
    set value [string index $msg 12]
    #    puts $value
    
    return $value
    
}



######################
# QSFP light functions
######################
# toggle the light
proc func_qsfpToggle {} {
    global wr_reg g_minor
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x80 |& grep -v info} msg   
}

# Check if the light is on or off
# 1 ON
# 0 OFF
proc func_checkQsfp {} {
    global rd_reg g_minor
    
    catch {exec $rd_reg -m$g_minor -a0a0 |& grep -v info | grep 0a0} msg
    set value [string index $msg 12]   
    puts "QSFP LIGHT $value"
    return $value   
}

############################
# FUNC to set the link speed
############################

# TPC RCU1 2.125 Gb/s
proc func_set2125Gb {} {
    global wr_reg rd_reg g_minor
    puts "Setting 2.125 Gb/s ...."
    catch {exec $wr_reg -m$g_minor -c0 -a0xcc -v0x0000000  |& grep -v info} msg
    puts "... 2.125 Gb/s ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x10  |& grep -v info} msg
    puts "... reset link ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x20 |& grep -v info} msg
    puts "... done"
}

# TPC RCU2 and CTP 3.125 Gb/s
proc func_set3125Gb {} {
    global wr_reg rd_reg g_minor
    puts "Setting 3.125 Gb/s ...."
    catch {exec $wr_reg -m$g_minor -c0 -a0xcc -v0x00020000 |& grep -v info} msg
    puts "... 3.125 Gb/s ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x10 |& grep -v info} msg
    puts "... reset link ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x20 |& grep -v info} msg
    puts "... done"
}

# TRD 4.0 Gb/s
proc func_set4Gb {} {
    global wr_reg rd_reg g_minor
    puts "Setting 4.0 Gb/s ...."
    catch {exec $wr_reg -m$g_minor -c0 -a0xcc -v0x00020000 |& grep -v info} msg
    puts "... 4.0 Gb/s ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x10 |& grep -v info} msg
    puts "... reset link ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x20 |& grep -v info} msg
    puts "... done"
}

# TPC RCU3 4.25 Gb/s ??
proc func_set425Gb {} {
    global wr_reg rd_reg g_minor
    puts "Setting 4.25 Gb/s ...."
    catch {exec $wr_reg -m$g_minor -c0 -a0xcc -v0x00010000 |& grep -v info} msg
    puts "... 4.25 Gb/s ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x10 |& grep -v info} msg
    puts "... reset link ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x20 |& grep -v info} msg
    puts "... done"
}

proc func_set53125Gb {} {
    global wr_reg rd_reg g_minor
    
    puts "Setting 5.3125 Gb/s ...."
    catch {exec $wr_reg -m$g_minor -c0 -a0xcc -v0x00030000 |& grep -v info} msg
    puts "... 5.3125 Gb/s ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x10 |& grep -v info} msg
    puts "... reset link ..."
    catch {exec $wr_reg -m$g_minor -c0 -a0xd0 -v0x20 |& grep -v info} msg
    puts "... done"
}


###############
# OSC functions
###############

# func to reset osc fre to default
proc func_resetOsc {} {
    global wr_reg g_id

    catch {exec $wr_reg --id=$g_id --channel=0 --address=0xd0 --value=0x4 |& grep -v info} msg
}
# func to read osc values
proc func_oscRead {} {
    global wr_reg g_id
    catch {exec $wr_reg --id=$g_id --channel=0 --address=0x0d0 --value=0x1 |& grep -v info} msg
}
# func to write osc values
proc func_oscWrite {} {
    global wr_reg g_id
    catch {exec $wr_reg --id=$g_id --channel=0 --address=0x0d0 --value=0x2 |& grep -v info} msg
}
# func to RST osc SM
proc func_oscRst {} {
    global wr_reg g_id
    catch {exec $wr_reg --id=$g_id --channel=0 --address=0x0d0 --value=0x40 |& grep -v info} msg
}

# read osc value from C-RORC reg
proc func_oscReadValue {} {
    global wr_reg rd_reg g_id osc_value

    func_oscRst
    func_oscRead
    catch {exec $rd_reg --id=$g_id --channel=0 --address=0x0c8 |& grep -v info} msg
    puts "0xC8 $msg"
    #regexp {\s-\s+(\w\w\w\w)\s(\w\w\w\w)} $msg whole_match first second
    set t1 [string range $msg 2 10]
    catch {exec $rd_reg --id=$g_id  --channel=0 --address=0x0cc |& grep -v info} msg
    puts "0xCC $msg"
    #regexp {\s-\s+(\w\w\w\w)\s(\w\w\w\w)} $msg whole_match third forth
    set t2 [string range $msg 2 10]
    #set tmp "$forth$first$second"
    set tmp "$t2$t1"
    return $tmp
}


# calculate the new OSC values
proc func_crorcWrOscillator {freq} {
    global osc_value g_new_osc_value g_osc_prg osc_value 
    
    set osc_value(0) [func_oscReadValue]
    # run osc program
    catch {exec $g_osc_prg $osc_value(0) $freq 4 7 0} msg
    # Set new values
    set i 0
    foreach val $msg {
	set g_new_osc_value($i) $val
	incr i
    }
    set osc_value(1) "$g_new_osc_value(0)$g_new_osc_value(1)"
    # load the new values
    func_oscWriteValues
}
# write new osc values in the C-RORC regs
proc func_oscWriteValues {} {
    global wr_reg rd_reg g_id g_new_osc_value
    
    func_oscRst
    catch {exec $wr_reg --id=$g_id --channel=0 --address=0x0c8 --value=0x$g_new_osc_value(1)  |& grep -v info} msg
    catch {exec $wr_reg --id=$g_id --channel=0 --address=0x0cc --value=0x$g_new_osc_value(0) |& grep -v info} msg
    func_oscWrite
}

# check the OSC values are correct
proc func_crorcRdOscillator {} {
    global osc_value

    set osc_value(2) [func_oscReadValue]
    #    puts $osc_value(2)
    # check the results
    if { "$osc_value(1)" == "$osc_value(2)" } {
	puts "OSC WELL CONFIGURED"
	puts " WROTE : $osc_value(1)"
	puts " READ  : $osc_value(2)"
	return 0
    } else {
	puts "SOMETHING WENT WRONG"
	puts " WROTE : $osc_value(1)"
	puts " READ  : $osc_value(2)"
	return 1
    }
}

###################################
# Check the status of all the links
# TPC check all 6 links
# TRD check only link 4 and 5
###################################
proc func_checkSiuStatus {} {
    global g_minor g_det

    set ch_failed 0
    if {"$g_det" == "TPC1" || "$g_det" == "TPC2" || "$g_det" == "TPC3" || "$g_det" == "TPC4"} {        
        set ch_start 0
        set ch_stop 6
    } elseif {"$g_det" == "TRD"} {
        set ch_start 4
        set ch_stop 6
    }
    
    puts "**** SIU LINK CHECKS ****"
    for {set ch $ch_start} {$ch < $ch_stop} {incr ch} {
        if [catch {exec /date/rorc/Linux/siu_status -m $g_minor -c $ch | grep serial} msg] {
            incr ch_failed
            after 100
        }

        puts "SIU ch $ch --> $msg"
    }
    puts "**** SIU LINK CHECKS (link down $ch_failed) ****"
    after 1000
    # that means some ch is down  
    return $ch_failed
}

############################
# func to check the DET CODE
############################
proc func_checkDetCode {} {
    global g_det

    # default
    if {"$g_det" == ""} {
        set g_det TPC1        
    }

    if {"$g_det" != "TPC1" && "$g_det" != "TPC2" && "$g_det" != "TPC3" && "$g_det" != "TPC4" && "$g_det" != "TRD" && "$g_det" != "HLT" && "$g_det" != "CTP" } {
        set g_det TPC1
    }
}

###############################
# Function to handle the option
###############################
proc HandleOption {} {
    global g_det g_minor g_siu_status
    set index 0
    foreach arg $::argv {
	set arg_opt($index) $arg
	incr index
    }
    set g_det $arg_opt(0)
    func_checkDetCode
    set g_minor $arg_opt(1)
    set g_siu_status $arg_opt(2)
}

proc InsertOption {} {
    global g_det g_minor g_siu_status
    
    # Set the DETECTOR CODE
    puts "Enter detector code : "
    puts "- TPC1 @ 2.1250 Gb/s \[default\]"
    puts "- TPC2 @ 3.1250 Gb/s"
    puts "- TPC3 @ 4.0000 Gb/s"
    puts "- TPC4 @ 4.2500 Gb/s"
    puts "- TRD  @ 4.0000 Gb/s"
    puts "- HLT  @ 5.3125 Gb/s"
    puts "- CTP  @ 3.1250 Gb/s"
    flush stdout
    gets stdin g_det
    func_checkDetCode
    puts "Detector selected : $g_det"
    # CRORC G_MINOR number
    puts -nonewline "Enter g_minor number of the C-RORC : "
    flush stdout
    gets stdin g_minor
    # CRORC checks of the link status
    puts -nonewline "Check SIU link \[1/0\]: "
    flush stdout
    gets stdin g_siu_status    
}

proc set_DEFAULT {} {
    global wr_reg rd_reg g_osc_prg siu_status MAX_RETRY_TIME
    global g_id 
    
    set wr_reg "roc-reg-write"
    set rd_reg "roc-reg-read"
    set g_osc_prg "./crorc_osc"
    set g_siu_status 1
    set MAX_RETRY_TIME 5

#    set g_id "3:0.0"
}

#######################
######### MAIN ########
#######################

set index 0

foreach arg $::argv {
    set arg_opt($index) $arg
    incr index
}
set g_id $arg_opt(0)


set_DEFAULT
#set g_minor 0 

func_resetOsc
func_crorcWrOscillator 240.0
if [func_crorcRdOscillator] {
    exit 1
}


puts "Config DONE !!! "
