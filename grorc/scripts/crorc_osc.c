#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define ORIG_FREQ 212.5

long long int value;
int n1,hs_div;
long long int rfreq = 0;

void hexa (char *p) {
  long long int value;
  int index;
  long long int hex = 1;

  int i;
  int temp;
  
  // HS_DIV
  index = 0;
  if (p[index] >='a') temp = p[index] - 'a' + 10;
  else temp = p[index] - '0';
  //  printf("TEMP %c %d\n",p[index],temp);
  hs_div = ((temp>>1) & 0x1) + ((temp>>2) & 0x1)*2 + ((temp>>3) & 0x1)*4;
  if (hs_div == 0) hs_div = 4;
  else if (1 == hs_div) hs_div = 5;
  else if (2 == hs_div) hs_div = 6;
  else if (3 == hs_div) hs_div = 7;
  else if (5 == hs_div) hs_div = 9;
  else if (7 == hs_div) hs_div = 11;
  //  printf ("HS_DIV: %d\n",hs_div);

  // N1
  index++;
  n1 = (temp & 0x1) * 64;
  if (p[index] >='a') temp = p[index] - 'a' + 10;
  else temp = p[index] - '0';
  //  printf("TEMP %c %d\n",p[index],temp);
  n1 += ((temp>>0) & 0x1)*4 + ((temp>>1) & 0x1)*8 + ((temp>>2) & 0x1)*16 + ((temp>>3) & 0x1)*32;
  index++;
  if (p[index] >='a') temp = p[index] - 'a' + 10;
  else temp = p[index] - '0';
  //  printf("TEMP %c %d\n",p[index],temp);
  n1 += ((temp>>3) & 0x1)*2 + ((temp>>2) & 0x1);
  n1++;
  //  printf ("N1: %d\n",n1);
  
  //RFREQ
  
  for (i=11;i>=3;i--) {
    //    printf("%ld %C\n",hex,p[i]);
    if (p[i] >='a') value = p[i] - 'a' + 10;
    else value = p[i] - '0';
    if (i<11) hex = hex*16;
    //    printf("%ld %C\n",hex,p[i]);
    value = value * hex;
    rfreq += value;
  }
  //  printf ("RFREQ: %ld %lX\n",rfreq,rfreq);

  
}

int main(int argc,char **argv) {
  double number, tot,tot_div, power;

  int n1_new,hs_div_new;
  int intg;
  double fract;
  double fdco,fdco_new,fxtal;
  double new_freq;
  
  double rfreq_new,rfreq_fract;
  long long int rfreq_fract_int;
  int rfreq_int;
  long long int rfreq_hex;

  int first,second,minor;
  
  if (argc < 6) {
    printf ("%s usage:\n",argv[0]);
    printf("%s OSC_VALUE NEW_FREQ N1 HS RORC_MINOR\n",argv[0]);
    exit(1);
  }

  hexa(argv[1]); 

  new_freq = atof(argv[2]);
  n1_new = atoi(argv[3]);
  hs_div_new = atoi(argv[4]);
  minor = atoi(argv[5]);

  //  printf ("\n\n********************\n");
  //  printf("OUTPUT FREQ\t%f\n", ORIG_FREQ);
  //  printf("N1\t%d\n",n1);
  //  printf("HS_DIV\t%d\n",hs_div);
  //  printf ("********************\n\n");

  //  printf ("\n\n****** NEW *********\n");
  //  printf("OUTPUT FREQ\t%f\n", new_freq);
  //  printf("N1\t%d\n",n1_new);
  //  printf("HS_DIV\t%d\n",hs_div_new);
  //  printf ("********************\n\n");


  power = pow(2.0,28.0);
  number = (double) rfreq;
  
  tot_div = number / power;
  
  intg = (int) tot_div;
  fract = tot_div - (double)intg;

  //  printf ("RFREQ\t%4.10lf \n",tot_div);
  
  fdco = (ORIG_FREQ * 1000000.0 * (double)n1 * (double)hs_div);
  //  printf ("FDCO\t%lf \n",fdco);

  fxtal = fdco / tot_div;

  //  printf ("FXTAL\t%4.10lf (%4.3lf MHz)\n",fxtal,fxtal/1000000.0);

  fdco_new = new_freq * (double)n1_new * (double) hs_div_new * 1000000.0;

  //  printf ("FDCO_N\t%lf (%lf GHz) -- 4.85 <==> 5.67\n",fdco_new,fdco_new/1000000000.0);

  rfreq_new = fdco_new / fxtal;

  //  printf ("RFREQ_N\t%4.10lf \n",rfreq_new);
  
  rfreq_int = (int)rfreq_new;
  rfreq_fract = rfreq_new - (double)rfreq_int;
  rfreq_fract = rfreq_fract * power;
  //  printf ("FRACT * power %lf \n", rfreq_fract);
  rfreq_fract_int = (long long int) rfreq_fract;
  
  //  printf ("%d %lld \n",rfreq_int,rfreq_fract_int);
  
  rfreq_hex = (long long int)rfreq_int << 28;
  rfreq_hex = rfreq_hex | rfreq_fract_int;
  //  printf("RFREQ_N\t%010llX\n",rfreq_hex);

  first = rfreq_hex & 0xffffffff;
  //  printf("\n*******\n%X\n",first);
  second = rfreq_hex >> 32 & 0x3f;

  if (hs_div_new == 4) hs_div_new = 0;
  else if (hs_div_new == 5) hs_div_new = 1;
  else if (hs_div_new == 6) hs_div_new = 2;
  else if (hs_div_new == 7) hs_div_new = 3;
  else if (hs_div_new == 9) hs_div_new = 5;
  else if (hs_div_new == 11) hs_div_new = 7;

  second = hs_div_new << 13 | ((n1_new-1)&0x7f) << 6 | second;
  //  printf("%X\n*******\n",second);

  //  printf ("\n\n");
  //  printf("./rorc_wr_reg -m%d -a0d0 -v0x40\n",minor);
  //  printf("./rorc_wr_reg -m%d -a0c8 -v%x\n",minor,first);
  //  printf("./rorc_wr_reg -m%d -a0cc -v%x\n",minor,second);
  //  printf("./rorc_wr_reg -m%d -a0d0 -v0x2\n",minor);
  //  printf("./rorc_wr_reg -m%d -a0d0 -v0x40\n",minor);
  //  printf("./rorc_wr_reg -m%d -a0d0 -v0x1\n",minor);
  //  printf("./rorc_reg -m%d -a0c8 \n",minor);
  //  printf("./rorc_reg -m%d -a0cc \n",minor);
  printf("%04x %08x\n",second,first);

  
  return 0;

}
