#!/bin/bash

# get directory where this script is
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

#cd /date/readList
tclsh osc_change.tcl $1
# gives something like
#0xC8 (0c8)- 1100 1010 0111 1100  1010 0101 0100 0001 - ca7c a541
#0xCC (0cc)- 0000 0000 0000 0000  0100 0000 1100 0010 - 0000 40c2
#0xC8 (0c8)- 1010 1101 0111 0001  0000 1001 1110 1111 - ad71 09ef
#0xCC (0cc)- 0000 0000 0000 0000  0110 0000 1100 0011 - 0000 60c3
#OSC WELL CONFIGURED
# WROTE : 60c3ad7109ef
# READ  : 60c3ad7109ef
#Config DONE !!!

#read -p "configure the GBTx chip using the master.txt file and press <enter> when ready"

#cd /date/rorc/Linux
#./rorc_read_reg -m0 -c0 -a0x1b8
# you will see something like
# add 0x1b8 => 0000 11ff
# which means
#      RX CLK READY [3:0] ==> F (each bank 4 links)
#      SERDES READY [7:4] ==> F
#      GBT RX READY [11:8] ==> 1 (only 1 fiber connected in ch1)
#      TX PLL ready [12] ==> 1 (it is in common with all 4 links)

#echo "you shall see 0000 11ff on the line above or you should restart sequence"

